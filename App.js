import React, { Component } from 'react';
import { Platform, StatusBar, I18nManager, BackHandler, Alert } from 'react-native';
import AppMain from './source/app';
import { MenuProvider } from 'react-native-popup-menu';
// import store from './source/Stores/orderStore';
// import Store from './source/Stores';
import ApiController from './source/ApiController/ApiController';
import { nav_header_color } from './styles/common';
import Toast from 'react-native-simple-toast';
import NetInfo from "@react-native-community/netinfo";

export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      color: 'rgba(0,153,0,0.9)',
      loading: false,
      connection_Status: ""
    };
    I18nManager.forceRTL(false);
  }
  fucn() {
    var timerId = setInterval(() => {
      if (1) {
        alert('leon')
        // this.setState({ loading: false })
        clearInterval(timerId);
      } else {
        console.warn('app.js')
      }
    }, 5000);
  }
  componentDidMount() {
    NetInfo.addEventListener(state => {
      'connectionChange';
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      this._handleConnectivityChange(state)

    });

    NetInfo.fetch().then((isConnected) => {
      
      if (isConnected.isConnected === true) {
        Toast.showWithGravity('You are online', Toast.LONG, Toast.TOP)
      }
      else {
        console.log('leon')
        Toast.showWithGravity('Kindly turn on the internet', Toast.LONG, Toast.TOP)
      }

    });
    setTimeout(() => { this.setState({ color: '#009900' }) }, 9000)
  }

  componentWillUnmount() {
    NetInfo.removeEventListener(state => {
      'connectionChange';
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      this._handleConnectivityChange(state)

    });
  }


  _handleConnectivityChange = (isConnected) => {

    if (isConnected.isConnected === true) {
      Toast.showWithGravity('You are online', Toast.LONG, Toast.TOP)
    }
    else {
      Toast.showWithGravity('Kindly turn on the internet', Toast.LONG, Toast.TOP)
    }
  };


  render() {
    return (
      <MenuProvider>
        <StatusBar
          hidden={false}
          animated={true}
          backgroundColor={this.state.color}
          barStyle="light-content"
          networkActivityIndicatorVisible={Platform.OS === 'ios' ? false : false}
          showHideTransition={Platform.OS === 'ios' ? 'slide' : null}
        />
        <AppMain />
      </MenuProvider>
    );
  }
}
