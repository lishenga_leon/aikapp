import React, { Component } from 'react';
import { StyleSheet, I18nManager } from 'react-native';
import { InputTextSize } from './common';
import { width, height, totalSize } from 'react-native-dimension';
const buttonTxt = 1.8;
const paragraphTxt = 1.5;
const headingTxt = 1.6;
const smallButtons = 1.2;
const titles = 1.8;
const styles = StyleSheet.create({


  searchIcon: {
    height: height(2.5),
    width: width(15),
    resizeMode: 'contain',
    alignSelf: 'center',
    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}],
    marginTop: height(1)
  },

  searchTouchIcon: {
    height: height(5),
    width: width(15),
    resizeMode: 'contain',
    alignSelf: 'center',
    marginLeft: -width(10),
    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]
  },
  searchCon: {
    height: height(5),
    backgroundColor: '#ffffff',
    borderRadius: 4,
    marginVertical: 2,
    flexDirection: 'row',
    alignItems:'center',
    marginBottom: height(1),
    width: width(95),
    marginRight: width(5)
  },

  container: {
    flex:1,
    alignItems: 'center',
    backgroundColor:'#ffffff'
  },
  mapCon: {
    height:height(35),
    width:width(100),
    alignSelf:'center',
    alignItems:'center'
  },
  map: {
    height:height(35),
    width:width(100),
    zIndex : -10,
    position: 'absolute',
  },
  txtInput: {
    fontSize: InputTextSize,
    width: width(90),
    height: height(5),
    margin: 2,
    paddingHorizontal: 5,
    textAlign: 'left',
    backgroundColor: 'rgba(0,0,0,0.7)',
    color: 'white',
    marginLeft: width(5)
  },
  
});

export default styles;
