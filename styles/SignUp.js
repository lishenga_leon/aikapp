import React, { Component } from 'react';
import { Platform,I18nManager,StyleSheet } from 'react-native';
import { COLOR_PRIMARY, socialBtnText,buttonText,SloganText,InputTextSize,SignInHeaderText, smallTitle, COLOR_SECONDARY } from './common';
import { width, height, totalSize } from 'react-native-dimension';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white'
  },
  imgCon: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center'
  },
  bckImgCon: {
    flex: 0.5,
    justifyContent: 'flex-end'
  },
  backBtn: {
    height: height(2.5),
    width: width(3),
    marginLeft: 25,
    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}],
  },
  headerTxt: {
    // fontFamily: FONT_NORMAL,
    fontWeight: 'bold',
    fontSize: SignInHeaderText,
    color: 'white',
    fontWeight: 'bold',
    alignSelf: 'flex-end',
    paddingEnd: 25,
  },
  logoView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: height(13)
  },

  limitedTxt: {
    // fontFamily: FONT_NORMAL,
    marginTop: height(5),
    width: width(50),
    fontSize: 12, //totalSize(S15)
    alignSelf: 'center',
    textAlign: 'center',
    color: COLOR_SECONDARY,
    marginVertical: 3
  },


  logoImg: {
    height: height(15),
    width: width(65),
    // resizeMode: 'contain'
  },
  logoTxt: {
    height: height(12),
    width: width(65),
    textAlign: 'center',
    // fontFamily: FONT_NORMAL,
    fontSize: SloganText,
    color: 'black',
    marginTop: 30
  },
  buttonView: {
    alignItems: 'center',
    marginTop: height(10)
  },
  userImg: {
    height: height(3.5),
    width: width(5.5),
    marginHorizontal: 15
  },
  mail: {
    height: height(5.5),
    width: width(5.5),
    marginHorizontal: 15
  },
  IndicatorSectionCon: {
    height: height(5),
    width: width(100),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  pickerCon: {
    height: height(6),
    width: width(90),
    marginVertical: 5,
    justifyContent: 'center',
  },
  textareaCon:{
    height: height(6),
    width: width(90),
    marginVertical: 5,
    justifyContent: 'center',
  },
  btn: {
    height: height(6),
    width: width(80),
    flexDirection: 'row',
    borderRadius: 3,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(172, 172, 172, 0.2)',
    borderColor: 'rgba(172, 172, 172, 0.5)',
    borderWidth: 1,
    marginTop: 5
  },
  btnTextarea:{
    height: height(12),
    width: width(80),
    flexDirection: 'row',
    borderRadius: 3,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(172, 172, 172, 0.2)',
    borderColor: 'rgba(172, 172, 172, 0.5)',
    borderWidth: 1,
    marginTop: 5
  },
  inputTxt: {
    alignSelf: 'stretch',
    height: height(6),
    textAlignVertical: 'center',
    fontSize: InputTextSize,
    textAlign: 'left',
    color: 'black',
  },
  loginErrorTxt: {
    fontWeight: 'bold',
    fontSize: buttonText,
    color: 'red',
  },
  socialBtnText: {
    fontSize: socialBtnText, color: 'white' 
  },
  signUpBtn: {
    height: height(6),
    width: width(80),
    marginTop: 10,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffc400'
  },
  buttonCon: {
    width: width(37.1), 
    height: height(5), 
    borderRadius: 3, 
    backgroundColor: '#134A7C', 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  signTxt: {
    // fontFamily: FONT_NORMAL,
    fontSize: buttonText,
    color: 'white',
    fontWeight: 'bold'
  },
  signUpTxt: {
    // fontWeight: 'bold',
    fontSize: buttonText,
    color: COLOR_PRIMARY,
  },
  fgBtn: {
    height: height(5.5),
    width: width(80),
    flexDirection: 'row',
    marginTop: 20
  },
  other: {
    height: height(5.5),
    width: width(29.5),
    resizeMode: 'contain',
  },
  orTxt: {
    // height:height(5.5),
    marginTop: 10,
    width: width(6),
    textAlign: 'center',
    // fontFamily: FONT_NORMAL,
    fontSize: 10,
    color: '#ffffff',
  },
  expTxt: {
    // fontFamily: FONT_NORMAL,
    fontSize: 12,
    color: 'black'
  },
  signUpT: {
    height: height(3),
    // fontFamily: FONT_BOLD,
    fontWeight: 'bold',
    fontSize: 12,
    textDecorationLine: 'underline',
    color: 'black',
  },
  footer: {
    height: height(7),
    width: width(65),
    alignContent: 'center'
  },
});

export default styles;
