import React, { Component } from 'react';
import {
  ActivityIndicator, Text, View, Image, TouchableOpacity, 
  I18nManager, TextInput, StyleSheet, ScrollView
} from 'react-native';
import { INDICATOR_SIZE, InputTextSize, COLOR_PRIMARY } from '../../../styles/common';
import { height, width } from 'react-native-dimension';
import { NavigationActions } from 'react-navigation';
import styles from '../../../styles/SignUp'
import IconMail from 'react-native-vector-icons/Octicons';
import IconLock from 'react-native-vector-icons/SimpleLineIcons';
import IconUser from 'react-native-vector-icons/FontAwesome';
import { Textarea, Icon } from 'native-base';
import SelectPicker from 'react-native-form-select-picker';
import { validate } from 'validate.js';
import constraints from '../../constraints';


const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    alignSelf: 'stretch',
    height: height(6),
    textAlignVertical: 'center',
    fontSize: InputTextSize,
    textAlign: 'left',
    color: COLOR_PRIMARY,
  },
});

export default class SignUp extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loadCat: true,
      name: '',
      email: '',
      password: '',
      category: '',
      enterpriceName: '',
      farmProduce: '',
      agriSections: [],
      data: { emailAddress: "example@gmail.com" },
      icon: 'eye-off',
      showPassword: true
    }
  }
  static navigationOptions = {
    header: null
  };
  navigateToScreen = (route, name, email, password, category, enterpriceName) => {
    const validationResult = validate(this.state.data, constraints);
    if (validationResult == undefined) {
      const navigateAction = NavigationActions.navigate({
        routeName: route,
        params: { name: name, email: this.state.data.emailAddress, password: password, category: category, enterpriceName }
      });
      this.props.navigation.setParams({ name: name, email: this.state.data.emailAddress, password: password, category: category, enterpriceName: enterpriceName });
      this.setState({
        name: '',
        email: '',
        category: '',
        password: '',
        enterpriceName: ''
      })
      this.props.navigation.dispatch(navigateAction);
    } else {
      // validationResult is undefined if there are no errors
      this.setState({ errors: validationResult });
    }
  }
  componentDidMount = async () => {
    await this.loadSections()
  }
  

  loadSections = async () => {
    headers = {
      Authorization: 'Bearer '
    }

    const response = await fetch('http://209.97.136.165:8600/api/listings/list/sections', {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client // body data type must match "Content-Type" header
    });

    result = await response.json();

    this.setState({
      agriSections: result.data.data,
      loadCat: false
    })
  }

  _changeIcon(){
    this.setState(prevState =>({
      icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
      showPassword: !prevState.showPassword
    }))
  }

  getErrorMessages(separator = "\n") {
    const { errors } = this.state;
    if (!errors) return [];

    return Object.values(errors).map(it => it.join(separator)).join(separator);
  }

  getErrorsInField(field) {
    const { errors } = this.state;
    return errors && errors[field] || [];
  }

  isFieldInError(field) {
    const { errors } = this.state;
    return errors && !!errors[field];
  }


  render() {

    const placeholder = {
      label: 'Category',
      value: null,
      color: 'black',
    };

    const entplaceholder = {
      label: 'Select Enterprise',
      value: null,
      color: 'black',
    };

    const farmplaceholder = {
      label: 'Select Produce',
      value: null,
      color: 'black',
    };

    return (
      <ScrollView style={styles.container}>
        <View style={{ height: height(5), flexDirection: 'row' }}>
          <TouchableOpacity style={styles.bckImgCon} onPress={() => this.props.navigation.goBack()}>
            <Image source={require('../../images/back_btn.png')} style={styles.backBtn} />
          </TouchableOpacity>
          <View style={{ flex: 0.5, justifyContent: 'flex-end', marginHorizontal: 25 }}>
          </View>
        </View>
        <View style={styles.logoView}>
          <Image source={require('../../images/images/AIK-App-Logo.png')} style={styles.logoImg} />
        </View>
        <View style={styles.buttonView}>
          <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
            <View style={{ marginHorizontal: 10 }}>
              <IconUser name='user-o' color='black' size={24} />
            </View>
            <View style={{ flex: 4.1 }}>
              <TextInput
                onChangeText={(value) => this.setState({ name: value })}
                underlineColorAndroid='transparent'
                placeholder='Your Name'
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                autoCorrect={true}
                autoFocus={false}
                keyboardAppearance='default'
                autoCompleteType='name'
                keyboardType='default'
                style={styles.inputTxt}
              />
            </View>
          </View>
          <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
            <View style={{ marginHorizontal: 10 }}>
              <IconMail name='mail' color='black' size={24} />
            </View>
            <View style={{ flex: 4.1 }}>
              <TextInput
                onChangeText={(value) => this.setState({
                  email: value.toLowerCase(), 
                  ...this.state,
                  data: {
                    ...this.state.data,
                    emailAddress: value.toLowerCase()
                  }
                })}
                underlineColorAndroid='transparent'
                placeholder='Your Email Address'
                placeholderTextColor='black'
                keyboardType='email-address'
                autoCompleteType='email'
                underlineColorAndroid='transparent'
                autoCorrect={true}
                style={styles.inputTxt}
              />
            </View>
          </View>

          {this.isFieldInError('emailAddress') && this.getErrorsInField('emailAddress').map(errorMessage => <Text style={styles.loginErrorTxt}>{errorMessage}</Text>)}

          <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
            <View style={{ marginHorizontal: 10 }}>
            </View>
            <View style={{ flex: 4.1 }}>
              <View style={styles.pickerCon}>
                <SelectPicker
                  onValueChange={(value) => {
                    // Do anything you want with the value. 
                    // For example, save in state.
                    this.setState({
                      category: value,
                    })
                  }}
                  selected={this.state.category}
                  placeholder='Category'
                  placeholderStyle={{ fontSize: 15, color: 'black' }}
                >
                  <SelectPicker.Item label="Enterprise" value="ent" />
                  <SelectPicker.Item label="Farmer" value="farmer" />

                </SelectPicker>
              </View>
            </View>
          </View>
          {
            this.state.category == 'ent' ?
              <View>
                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                  <View style={{ marginHorizontal: 10 }}>
                  </View>
                  {
                    this.state.loadCat ?
                      <View style={styles.IndicatorSectionCon}>
                        <ActivityIndicator color='red' size='small' animating={true} />
                      </View>
                      :

                      <View style={{ flex: 4.1 }}>
                        <View style={styles.pickerCon}>
                          <SelectPicker
                            onValueChange={(value) => {
                              // Do anything you want with the value. 
                              // For example, save in state.
                              this.setState({
                                enterpriceName: value
                              })
                            }}
                            selected={this.state.enterpriceName}
                            placeholder='Select Enterprise'
                            placeholderStyle={{ fontSize: 15, color: 'black' }}
                            onSelectedStyle={{ width:width(70) }}
                          >
                            {
                              this.state.agriSections.map((item, key) => {
                                return (
                                  <SelectPicker.Item label={item.name} value={item.id} />
                                )
                              })
                            }

                          </SelectPicker>
                        </View>
                      </View>
                  }
                </View>
              </View>
              : null
          }

          {
            this.state.category == 'farmer' ?
              <View>
                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                  <View style={{ marginHorizontal: 10 }}>
                  </View>
                  <View style={{ flex: 4.1 }}>
                    <View style={styles.pickerCon}>
                      <SelectPicker
                        onValueChange={(value) => {
                          // Do anything you want with the value. 
                          // For example, save in state.
                          this.setState({
                            farmProduce: value
                          })
                        }}
                        selected={this.state.farmProduce}
                        placeholder='Select Farm Produce'
                        placeholderStyle={{ fontSize: 15, color: 'black' }}
                      >
                        <SelectPicker.Item label='Livestock' value='live' />
                        <SelectPicker.Item label='Crops' value='crops' />
                        <SelectPicker.Item label='Fisheries' value='fish' />
                        <SelectPicker.Item label='Others' value='other' />

                      </SelectPicker>
                    </View>
                  </View>
                </View>
                {
                  this.state.farmProduce == 'other' ?
                    <View style={styles.btnTextarea} onPress={() => { this.props.navigation.navigate('Login') }}>
                      <View style={{ marginHorizontal: 10 }}>
                      </View>
                      <View style={{ flex: 4.1 }}>
                        <View style={styles.pickerCon}>
                          <Textarea
                            rowSpan={3}
                            placeholderTextColor="gray"
                            keyboardType='default'
                            maxLength={100000}
                            placeholder="Kindly Specify Your Produce"
                          />
                        </View>
                      </View>
                    </View>
                    : null
                }
              </View>
              : null
          }

          <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
            <View style={{ marginHorizontal: 10 }}>
              <IconLock name='lock' color='black' size={24} />
            </View>
            <View style={{ flex: 4.1 }}>
              <TextInput
                onChangeText={(value) => this.setState({ password: value })}
                underlineColorAndroid='transparent'
                placeholder='Your Password'
                secureTextEntry={this.state.showPassword}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                autoCompleteType='password'
                keyboardType='default'
                autoCorrect={false}
                style={[styles.inputTxt, { textAlign: I18nManager.isRTL ? 'right' : 'left' }]}
              />
            </View>
            <View style={{ marginHorizontal: 10 }}>
              <Icon name={this.state.icon} color='black' size={24} onPress={() => this._changeIcon()} />
            </View>
          </View>

          <TouchableOpacity style={[styles.signUpBtn, { backgroundColor: 'green' }]} onPress={() => { this.navigateToScreen('SignUpPage2', this.state.name, this.state.email, this.state.password, this.state.category, this.state.enterpriceName) }} >
            <Text style={styles.signUpTxt}>Sign Up</Text>
          </TouchableOpacity>
          <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
            {!this.state.loading ? null :
              <ActivityIndicator size={INDICATOR_SIZE} color='red' animating={true} hidesWhenStopped={true} />}
          </View>
        </View>
      </ScrollView>
    );
  }
}
