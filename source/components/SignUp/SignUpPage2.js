import React, { Component } from 'react';
import { 
    ActivityIndicator, Text, View, Image,
    TouchableOpacity, TextInput, ScrollView
} from 'react-native';
import { INDICATOR_SIZE } from '../../../styles/common';
import { height, width } from 'react-native-dimension';
import styles from '../../../styles/SignUp'
import IconLock from 'react-native-vector-icons/Entypo';
import IconUser from 'react-native-vector-icons/FontAwesome';
import IconPhone from 'react-native-vector-icons/Foundation';
import LocalDB from '../../LocalDB/LocalDB';
import SelectPicker from 'react-native-form-select-picker';
import Toast from 'react-native-simple-toast';


export default class SignUpPage2 extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            name: '',
            email: '',
            password: '',
            category: '',
            enterpriceName: '',
            farmProduce: '',
            enterCategory: [],

        }
        // I18nManager.forceRTL(data.is_rtl);
    }
    static navigationOptions = {
        header: null
    };
    componentDidMount = async () => {
        if (this.props.navigation.getParam('enterpriceName', 'SignUpPage2')) {
            await this.loadCategorySections()
        }
    }
    loadCategorySections = async () => {
        const options = {};
        //API calling
        // let res = await axios.get('http://209.97.136.165:8600/api/listings/list/subcategories/' + this.props.navigation.getParam('enterpriceName', 'SignUpPage2'), options)

        const response = await fetch('http://209.97.136.165:8600/api/listings/list/subcategories/' + this.props.navigation.getParam('enterpriceName', 'SignUpPage2'), {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client // body data type must match "Content-Type" header
        });

        result = await response.json();

        // console.log(res.data.data.data)
        this.setState({
            enterCategory: result.data.data,
        })
    }

    register = async () => {
        this.setState({ loading: true })
        let params = {
            name: this.props.navigation.getParam('name', 'SignUpPage2'),
            email: this.props.navigation.getParam('email', 'SignUpPage2'),
            password: this.props.navigation.getParam('password', 'SignUpPage2'),
            confirm_password: this.props.navigation.getParam('password', 'SignUpPage2'),
        }


        const response = await fetch('http://209.97.136.165:8600/api/auth/signup', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client 
            body: JSON.stringify(params) // body data type must match "Content-Type" header
        });

        result = await response.json();

        console.log(result)

        if (result.status == 200) {
            // store.login.loginStatus = true;
            // store.LOGIN_TYPE = 'local';
            await LocalDB.saveProfile(this.props.navigation.getParam('email', 'SignUpPage2'), this.props.navigation.getParam('password', 'SignUpPage2'), result.data);
            // store.login.loginResponse = response;
            this.setState({
                loading: false,
                name: '',
                email: '',
                password: '',
                category: '',
                enterpriceName: '',
                farmProduce: '',
                enterCategory: []
            })
            this.props.navigation.replace('Drawer');
        } else {
            this.setState({ loading: false })
            Toast.showWithGravity('Kindly try again', Toast.LONG, Toast.TOP)
        }
    }
    render() {

        const placeholder = {
            label: 'Category',
            value: null,
            color: 'black',
        };

        const entCategoryplaceholder = {
            label: 'Select Enterprise Category',
            value: null,
            color: 'black',
        };

        const farmplaceholder = {
            label: 'Select Produce',
            value: null,
            color: 'black',
        };

        return (

            <ScrollView style={styles.container}>


                {
                    this.props.navigation.getParam('category', 'SignUpPage2') == 'ent' ?
                        <View>
                            <View style={{ height: height(5), flexDirection: 'row' }}>
                                <TouchableOpacity style={styles.bckImgCon} onPress={() => this.props.navigation.goBack()}>
                                    <Image source={require('../../images/back_btn.png')} style={styles.backBtn} />
                                </TouchableOpacity>
                                <View style={{ flex: 0.5, justifyContent: 'flex-end', marginHorizontal: 25 }}>
                                </View>
                            </View>
                            <View style={styles.logoView}>
                                <Image source={require('../../images/images/AIK-App-Logo.png')} style={styles.logoImg} />
                            </View>
                            <View style={styles.buttonView}>
                                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                    </View>
                                    <View style={{ flex: 4.1 }}>
                                        <SelectPicker
                                            onValueChange={(value) => {
                                                // Do anything you want with the value. 
                                                // For example, save in state.
                                                this.setState({
                                                    category: value
                                                })
                                            }}
                                            selected={this.state.category}
                                            placeholder='Select Enterprise Category'
                                            placeholderStyle={{ fontSize: 15, color: 'black' }}
                                            onSelectedStyle={{ width:width(70) }}
                                        >
                                            {
                                                this.state.enterCategory.map((item, key) => {
                                                    return (
                                                        <SelectPicker.Item label={item.name} value={item.id} />
                                                    )
                                                })
                                            }

                                        </SelectPicker>
                                    </View>
                                </View>
                                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <IconLock name='address' color='black' size={24} />
                                    </View>
                                    <View style={{ flex: 4.1 }}>
                                        <TextInput
                                            onChangeText={(value) => this.setState({ physicaladdress: value })}
                                            underlineColorAndroid='transparent'
                                            placeholder='Physical Address'
                                            placeholderTextColor='black'
                                            underlineColorAndroid='transparent'
                                            autoCorrect={true}
                                            autoFocus={false}
                                            keyboardAppearance='default'
                                            keyboardType='default'
                                            style={styles.inputTxt}
                                        />
                                    </View>
                                </View>
                                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <IconLock name='address' color='black' size={24} />
                                    </View>
                                    <View style={{ flex: 4.1 }}>
                                        <TextInput
                                            onChangeText={(value) => this.setState({ postaladdress: value })}
                                            underlineColorAndroid='transparent'
                                            placeholder='Postal Address'
                                            placeholderTextColor='black'
                                            underlineColorAndroid='transparent'
                                            autoCorrect={true}
                                            autoFocus={false}
                                            keyboardAppearance='default'
                                            keyboardType='default'
                                            autoCompleteType='postal-code'
                                            style={styles.inputTxt}
                                        />
                                    </View>
                                </View>
                                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <IconPhone name='telephone' color='black' size={24} />
                                    </View>
                                    <View style={{ flex: 4.1 }}>
                                        <TextInput
                                            onChangeText={(value) => this.setState({ postaladdress: value })}
                                            underlineColorAndroid='transparent'
                                            placeholder='Telephone Number'
                                            placeholderTextColor='black'
                                            underlineColorAndroid='transparent'
                                            autoCorrect={true}
                                            autoFocus={false}
                                            keyboardAppearance='default'
                                            keyboardType='phone-pad'
                                            style={styles.inputTxt}
                                            autoCompleteType='tel'
                                        />
                                    </View>
                                </View>

                                <TouchableOpacity style={[styles.signUpBtn, { backgroundColor: 'green' }]} onPress={() => this.register()} >
                                    <Text style={styles.signUpTxt}>Complete Sign Up</Text>
                                </TouchableOpacity>
                                <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                                    {!this.state.loading ? null :
                                        <ActivityIndicator size={INDICATOR_SIZE} color='red' animating={true} hidesWhenStopped={true} />}
                                </View>
                            </View>
                        </View> :
                        <View>
                            <View style={{ height: height(5), flexDirection: 'row' }}>
                                <TouchableOpacity style={styles.bckImgCon} onPress={() => this.props.navigation.goBack()}>
                                    <Image source={require('../../images/back_btn.png')} style={styles.backBtn} />
                                </TouchableOpacity>
                                <View style={{ flex: 0.5, justifyContent: 'flex-end', marginHorizontal: 25 }}>
                                </View>
                            </View>
                            <View style={styles.logoView}>
                                <Image source={require('../../images/images/AIK-App-Logo.png')} style={styles.logoImg} />
                            </View>
                            <View style={styles.buttonView}>
                                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <IconUser name='user-o' color='black' size={24} />
                                    </View>
                                    <View style={{ flex: 4.1 }}>
                                        <TextInput
                                            onChangeText={(value) => this.setState({ name: value })}
                                            underlineColorAndroid='transparent'
                                            placeholder='Type of Crop/Livestock'
                                            placeholderTextColor='black'
                                            underlineColorAndroid='transparent'
                                            autoCorrect={true}
                                            autoFocus={false}
                                            keyboardAppearance='default'
                                            keyboardType='default'
                                            style={styles.inputTxt}
                                        />
                                    </View>
                                </View>
                                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <IconUser name='list-ul' color='black' size={24} />
                                    </View>
                                    <View style={{ flex: 4.1 }}>
                                        <TextInput
                                            onChangeText={(value) => this.setState({ name: value })}
                                            underlineColorAndroid='transparent'
                                            placeholder='Average'
                                            placeholderTextColor='black'
                                            underlineColorAndroid='transparent'
                                            autoCorrect={true}
                                            autoFocus={false}
                                            keyboardAppearance='default'
                                            keyboardType='numeric'
                                            style={styles.inputTxt}
                                        />
                                    </View>
                                </View>
                                <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
                                    <View style={{ marginHorizontal: 10 }}>
                                        <IconUser name='list-ol' color='black' size={24} />
                                    </View>
                                    <View style={{ flex: 4.1 }}>
                                        <TextInput
                                            onChangeText={(value) => this.setState({ name: value })}
                                            underlineColorAndroid='transparent'
                                            placeholder='N.O of Livestock'
                                            placeholderTextColor='black'
                                            underlineColorAndroid='transparent'
                                            autoCorrect={true}
                                            autoFocus={false}
                                            keyboardAppearance='default'
                                            keyboardType='numeric'
                                            style={styles.inputTxt}
                                        />
                                    </View>
                                </View>

                                <TouchableOpacity style={[styles.signUpBtn, { backgroundColor: 'green' }]} onPress={() => this.register()} >
                                    <Text style={styles.signUpTxt}>Complete Sign Up</Text>
                                </TouchableOpacity>
                                <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                                    {!this.state.loading ? null :
                                        <ActivityIndicator size={INDICATOR_SIZE} color='red' animating={true} hidesWhenStopped={true} />}
                                </View>
                            </View>
                        </View>
                }
            </ScrollView>
        );


    }
}
