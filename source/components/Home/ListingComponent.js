import React, { Component } from 'react';
import { Text, View, ImageBackground, TouchableOpacity } from 'react-native';
import { width } from 'react-native-dimension';
import { Icon } from 'react-native-elements';
import StarRating from 'react-native-star-rating';
import { COLOR_GRAY, COLOR_ORANGE } from '../../../styles/common';
import { withNavigation } from 'react-navigation';
import styles from '../../../styles/Home';
// import store from '../../Stores/orderStore';


class ListingComponent extends Component<Props> {
  
  render() {
    let item = this.props.item;
    let status = this.props.listStatus;

    if (item.contact_person_name !== null && item.contact_person_name !== "NULL") {
      return (
        <TouchableOpacity style={[styles.featuredFLItem, { width: status ? width(95) : width(90) }]} onPress={() => { this.props.navigation.navigate('FeatureDetailTabBar', { item: item.listing_id, list_title: item.company_name, item: item }) }}>
          {
            item.category_id == 305 ?
              <ImageBackground source={require('../../images/images/Agrodealer-icon.png')} style={styles.featuredImg}>
              </ImageBackground> :
              item.category_id == 126 ?
                <ImageBackground source={require('../../images/images/fertiliser-icon.png')} style={styles.featuredImg}>
                </ImageBackground> :
                item.category_id == 122 ?
                  <ImageBackground source={require('../../images/images/Machinery-icon.png')} style={styles.featuredImg}>
                  </ImageBackground> :
                  item.category_id == 105 ?
                    <ImageBackground source={require('../../images/images/agronomists-icon.png')} style={styles.featuredImg}>
                    </ImageBackground> :
                    item.category_id == 304 ?
                      <ImageBackground source={require('../../images/images/Vets-icon.png')} style={styles.featuredImg}>
                      </ImageBackground> :
                      item.category_id == 106 ?
                        <ImageBackground source={require('../../images/images/animal-feeds-icon.png')} style={styles.featuredImg}>
                        </ImageBackground> :
                        item.category_id == 135 ?
                          <ImageBackground source={require('../../images/images/Irrigation-icon.png')} style={styles.featuredImg}>
                          </ImageBackground> :
                          item.category_id == 147 ?
                            <ImageBackground source={require('../../images/images/seeds-icon.png')} style={styles.featuredImg}>
                            </ImageBackground>
                            : <ImageBackground source={{ uri: 'http://drommonster.co.za/wp-content/uploads/2018/02/Blog2.jpg'}} style={styles.featuredImg}>
                            </ImageBackground>
          }
          <View style={styles.txtViewCon}>

            {
              item.contact_person_name !== null && item.contact_person_name !== "NULL" ?
                <View style={{ width: width(50), alignItems: 'flex-start' }}>
                  <Text style={styles.txtViewHeading}>{item.contact_person_name}</Text>
                </View>
                : null
            }
            {
              item.contact_person !== null && item.contact_person !== "NULL" ?
                <View style={{ marginTop: 2, width: width(45), marginHorizontal: 8, flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.closedBtnTxt}>{item.contact_person}</Text>
                </View>
                : null
            }
            {
              item.email_address !== null && item.email_address !== "NULL" ?
                <View style={{ marginTop: 2, width: width(45), marginHorizontal: 8, flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.closedBtnTxt}>{item.email_address}</Text>
                </View>
                : null
            }
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity style={[styles.featuredFLItem, { width: status ? width(95) : width(90) }]} onPress={() => { this.props.navigation.navigate('FeatureDetailTabBar', { item: item.listing_id, list_title: item.company_name, item: item }) }}>
          {
            item.category_id == 305 ?
              <ImageBackground source={require('../../images/images/Agrodealer-icon.png')} style={styles.featuredImg}>
              </ImageBackground> :
              item.category_id == 126 ?
                <ImageBackground source={require('../../images/images/fertiliser-icon.png')} style={styles.featuredImg}>
                </ImageBackground> :
                item.category_id == 122 ?
                  <ImageBackground source={require('../../images/images/Machinery-icon.png')} style={styles.featuredImg}>
                  </ImageBackground> :
                  item.category_id == 105 ?
                    <ImageBackground source={require('../../images/images/agronomists-icon.png')} style={styles.featuredImg}>
                    </ImageBackground> :
                    item.category_id == 304 ?
                      <ImageBackground source={require('../../images/images/Vets-icon.png')} style={styles.featuredImg}>
                      </ImageBackground> :
                      item.category_id == 106 ?
                        <ImageBackground source={require('../../images/images/animal-feeds-icon.png')} style={styles.featuredImg}>
                        </ImageBackground> :
                        item.category_id == 135 ?
                          <ImageBackground source={require('../../images/images/Irrigation-icon.png')} style={styles.featuredImg}>
                          </ImageBackground> :
                          item.category_id == 147 ?
                            <ImageBackground source={require('../../images/images/seeds-icon.png')} style={styles.featuredImg}>
                            </ImageBackground>
                            : <ImageBackground source={{ uri: 'http://drommonster.co.za/wp-content/uploads/2018/02/Blog2.jpg'}} style={styles.featuredImg}>
                            </ImageBackground>
          }
          <View style={styles.txtViewCon}>

            {
              item.company_name !== null && item.company_name !== "NULL" ?
                <View style={{ width: width(50), alignItems: 'flex-start' }}>
                  <Text style={styles.txtViewHeading}>{item.company_name}</Text>
                </View>
                : null
            }
            {
              item.phone_number !== null && item.phone_number !== "NULL" ?
                <View style={{ marginTop: 2, width: width(45), marginHorizontal: 8, flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.closedBtnTxt}>{item.phone_number}</Text>
                </View>
                : null
            }
            {
              item.email_address !== null && item.email_address !== "NULL" ?
                <View style={{ marginTop: 2, width: width(45), marginHorizontal: 8, flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={styles.closedBtnTxt}>{item.email_address}</Text>
                </View>
                : null
            }
          </View>
        </TouchableOpacity>
      );

    }
  }
}
export default withNavigation(ListingComponent)
