import React, { Component } from 'react';
import {
  SafeAreaView, Text, View, Image, Dimensions, DeviceEventEmitter,
  TouchableOpacity, ScrollView, TextInput, FlatList, ActivityIndicator, StyleSheet
} from 'react-native';
import { width, height } from 'react-native-dimension';
import { NavigationActions } from 'react-navigation';
import * as Animatable from 'react-native-animatable';
import { InputTextSize } from '../../../styles/common';
import styles from '../../../styles/Home';
import Toast from 'react-native-simple-toast';
import SelectPicker from 'react-native-form-select-picker';
import { SliderBox } from "react-native-image-slider-box";
import LocalDB from '../../LocalDB/LocalDB';
import Form from 'react-native-vector-icons/AntDesign';
import PushNotification from 'react-native-push-notification'
import PushNotificationAndroid from 'react-native-push-notification'
import axios from 'axios';


const home_events = [
  {
    id: 305,
    event_category_name: 'Agrodealer',
    event_title: 'Agrodealer',
    enterprise: 'Agro-Business Enterprises',
    image: require('../../images/images/Agrodealer-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Kitale, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
  {
    id: 126,
    event_category_name: 'Fertilizer',
    event_title: 'Fertilizer',
    enterprise: 'Floriculture supplies & inputs',
    image: require('../../images/images/fertiliser-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Trans Nzoia Western, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
  {
    id: 122,
    event_category_name: 'Machinery',
    event_title: 'Machinery',
    enterprise: 'Motor vehicle dealers',
    image: require('../../images/images/Machinery-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Uasin Gishu Eldoret, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
  {
    id: 105,
    event_category_name: 'Agronomist',
    event_title: 'Agronomist',
    enterprise: 'Agro-Business Enterprises',
    image: require('../../images/images/agronomists-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Kakamega, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
  {
    id: 304,
    event_category_name: 'Veterinarian',
    event_title: 'Veterinarian',
    enterprise: 'Veterinary Clinics',
    image: require('../../images/images/Vets-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Kericho, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
  {
    id: 106,
    event_category_name: 'Animal Feeds',
    event_title: 'Animal Feeds',
    enterprise: 'Agro-Business Enterprises',
    image: require('../../images/images/animal-feeds-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Nyeri, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
  {
    id: 135,
    event_category_name: 'Irrigation',
    event_title: 'Irrigation',
    enterprise: 'Irrigation companies',
    image: require('../../images/images/Irrigation-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Marigat Baringo, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
  {
    id: 147,
    event_category_name: 'Seeds',
    event_title: 'Seeds',
    enterprise: 'Seed companies',
    image: require('../../images/images/seeds-icon.png'),
    color_code: 'red',
    business_hours_status: '8:00 am - 5:00pm',
    event_loc: 'Nairobi, Kenya',
    rating_stars: 4,
    total_views: 2,
    event_start_date: '12 / 12 / 2014',
    event_end_date: '12 / 12 / 2014',
  },
];

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: InputTextSize,
    width: width(80),
    height: 42,
    margin: 2,
    paddingHorizontal: 5,
    textAlign: 'left',
    backgroundColor: 'white',
    marginLeft: width(5)
  },
});


export default class Home extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      searchDropdown: false,
      activeSections: [],
      enterCategory: '',
      enterpriceName: '',
      agriSections: false,
      agriCategory: [],
      agriCategoryStatus: true,
      agroName: '',
      categoryId: '',
      subcategoryId: '',
      token: null,
      loadCat: false,
      images: [
        require('../../images/AIKIntroduction.png'),
        require('../../images/CASE-Toyota.png'),          // Local image
        require('../../images/ExpoAdvert.png'),
        require('../../images/Hortipro.png'),
        require('../../images/XylonMotors.png'),
      ]
    }
  }
  static navigationOptions = { header: null };
  navigateToScreen = (route, title, categoryId, agroName, subcategoryId) => {

    this.setState({
      categoryId: '',
      agroName: '',
      subcategoryId: '',
      enterpriceName: '',
      enterCategory: ''
    })


    if (title == 'Animal Feeds') {
      const navigateAction = NavigationActions.navigate({
        routeName: 'Animal',
        params: { otherParam: title, categoryId: categoryId, agroName: agroName, subcategoryId: subcategoryId }
      });
      this.props.navigation.setParams({ otherParam: title, categoryId: categoryId, agroName: agroName, subcategoryId: subcategoryId });
      this.props.navigation.dispatch(navigateAction);
    } else {
      const navigateAction = NavigationActions.navigate({
        routeName: title,
        params: { otherParam: title, categoryId: categoryId, agroName: agroName, subcategoryId: subcategoryId }
      });
      this.props.navigation.setParams({ otherParam: title, categoryId: categoryId, agroName: agroName, subcategoryId: subcategoryId });
      this.props.navigation.dispatch(navigateAction);
    }
  }

  componentDidMount = async () => {
    await this.loadSections()
    var status = await LocalDB.getNotificationStatus()
    console.log(status)
    if (JSON.parse(status['status']) == 1) {
      LocalDB.notificationStatus(JSON.stringify(0), null)
      this.props.navigation.navigate('Descriptions', { data: status['data'] })
    } else {
      PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
          console.log('TOKEN:', token)
          const params = {
            user_id: 0,
            token: token.token
          }

          console.log(params)

          axios.post('http://209.97.136.165:8600/api/push/register', params)
            .then(function (response) {
              console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            });
        },
        // (required) Called when a remote or local notification is opened or received
        onNotification: function (notification) {
          console.log('REMOTE NOTIFICATION ==>', notification)
          // process the notification here
          LocalDB.notificationStatus(JSON.stringify(1), notification['data'])
        },
        // Android only: GCM or FCM Sender ID
        senderID: '369360035878',
        popInitialNotification: false,
        requestPermissions: true
      })

      // console.log(PushNotificationAndroid.registerNotificationActions(['Accept', 'Reject', 'Yes', 'No']))

      // PushNotificationAndroid.registerNotificationActions(['Accept', 'Reject', 'Yes', 'No']);
      // DeviceEventEmitter.addListener('notificationActionReceived', function (action) {
      //   console.log('Notification action received: ' + action);
      //   const info = JSON.parse(action.dataJSON);
      //   if (info.action == 'Yes') {
      //     console.log('yes')
      //     // Do work pertaining to Accept action here
      //   } else if (info.action == 'No') {
      //     console.log('no')
      //     // Do work pertaining to Reject action here
      //   }
      //   // Add all the required actions handlers
      // });
    }


  }

  loadSections = async () => {
    token = await LocalDB.getUserProfile()
    if (token !== null) {

      const response = await fetch('http://209.97.136.165:8600/api/listings/list/sections', {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token.token
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *client // body data type must match "Content-Type" header
      });

      result = await response.json();

      this.setState({
        agriSections: result.data.data,
        token: token
      })
    } else {
      this.setState({
        agriSections: []
      })
      Toast.show('Kindly try again later', Toast.LONG, Toast.TOP)
    }
  }

  loadCategorySections = async (item) => {

    this.setState({
      enterpriceName: item.name,
      categoryId: item.id,
      loadCat: true
    })

    token = await LocalDB.getUserProfile()

    const response = await fetch('http://209.97.136.165:8600/api/listings/list/subcategories/' + item.id, {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.token
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client // body data type must match "Content-Type" header
    });

    result = await response.json();

    if (result.status == 200) {
      if (!Array.isArray(result.data.data) || !result.data.data.length) {
        this.setState({
          loadCat: false
        })
        this.navigateToScreen('SearchingScreen', 'Agrodealer', this.state.categoryId, this.state.agroName, this.state.subcategoryId)
      } else {
        this.setState({
          agriCategory: result.data.data,
          agriCategoryStatus: false,
          loadCat: false
        })
      }
    } else {
      this.setState({ loadCat: false, agriCategoryStatus: false, })
      Toast.show('Kindly try again later', Toast.LONG, Toast.TOP)
    }
  }


  _renderSectionTitle = section => {
    return (
      <View style={styles.content}>
        <Text>{section.content}</Text>
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <View style={[styles.header], { height: height(5) }}>
        <Text style={styles.headerText, { marginTop: height(1), marginLeft: width(2), color: 'white' }}>{section.title}</Text>
      </View>
    );
  };

  _renderContent = section => {

    const entplaceholder = {
      label: 'Select Enterprice',
      value: null,
      color: 'black',
    };

    const categoryplaceholder = {
      label: 'Select Category',
      value: null,
      color: 'black',
    };

    return (
      <View style={styles.content}>
        <TextInput
          onChangeText={(value) => this.setState({ email: value })}
          underlineColorAndroid='transparent'
          placeholder='Name'
          placeholderTextColor='black'
          underlineColorAndroid='transparent'
          autoCorrect={false}
          // onFocus={() => this.navigateToScreen('SearchingScreen')}
          style={styles.txtInput}
        />
        <SelectPicker
          onValueChange={(value) => {
            // Do anything you want with the value. 
            // For example, save in state.
            this.setState({
              enterpriceName: value
            })
          }}
          selected={this.state.category}
          placeholder='Enterprise'
          onSelectedStyle={{ width: width(70) }}
          placeholderStyle={{ fontSize: 15, color: 'black' }}
          style={styles.txtInput}
        >
          <SelectPicker.Item label='Agro-Business Enterprises' value='agrobiz' />
          <SelectPicker.Item label='Horticulture and Floriculture' value='hortflori' />
          <SelectPicker.Item label='Aggregators and Finance Institutions' value='agregafin' />
          <SelectPicker.Item label='Veterinary Services' value='vet' />
          <SelectPicker.Item label='Support Services' value='sup' />

        </SelectPicker>
        <SelectPicker
          onValueChange={(value) => {
            // Do anything you want with the value. 
            // For example, save in state.
            this.setState({
              enterpriceName: value
            })
          }}
          selected={this.state.category}
          placeholder='Category'
          placeholderStyle={{ fontSize: 15, color: 'black' }}
          style={styles.txtInput}
        >
          <SelectPicker.Item label='Agro-Business Enterprises' value='agrobiz' />
          <SelectPicker.Item label='Horticulture and Floriculture' value='hortflori' />
          <SelectPicker.Item label='Aggregators and Finance Institutions' value='agregafin' />
          <SelectPicker.Item label='Veterinary Services' value='vet' />
          <SelectPicker.Item label='Support Services' value='sup' />

        </SelectPicker>
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };



  render() {
    if (this.state.loading == true) {
      return (
        <View style={styles.IndicatorCon}>
          <ActivityIndicator color='red' size='large' animating={true} />
        </View>
      );
    }
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.subCon}>
          <ScrollView
            showsVerticalScrollIndicator={false}>
            <View style={styles.topViewCon}>
              <View style={styles.InnerRadius}>
                <View style={styles.imageCon}>
                  <View style={{ height: height(30), alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.7)' }}>
                    <SliderBox
                      images={this.state.images}
                      sliderBoxHeight={height(30)}
                      onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                      dotColor="transparent"
                      inactiveDotColor="#90A4AE"
                      paginationBoxVerticalPadding={20}
                      autoplay
                      circleLoop
                      resizeMethod={'resize'}
                      resizeMode={'cover'}
                      paginationBoxStyle={{
                        position: "absolute",
                        bottom: 0,
                        padding: 0,
                        alignItems: "center",
                        alignSelf: "center",
                        justifyContent: "center",
                        paddingVertical: 10
                      }}
                      dotStyle={{
                        width: 0,
                        height: 0,
                        borderRadius: 0,
                        marginHorizontal: 0,
                        padding: 0,
                        margin: 0,
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            {
              <View style={{ marginTop: -height(2), height: height(55) }}>
                <View style={{
                  // backgroundColor: 'rgba(0,0,0,0.7)',
                  width: width(90),
                  borderRadius: 4,
                  marginVertical: 2,
                  // flexDirection: 'row',
                  // alignItems: 'center',
                  marginTop: height(4)
                }}>
                  <View style={styles.searchCon}>
                    <TextInput
                      onChangeText={(value) => this.setState({ agroName: value })}
                      underlineColorAndroid='transparent'
                      placeholder='Name'
                      placeholderTextColor='white'
                      underlineColorAndroid='transparent'
                      autoCorrect={false}
                      // onFocus={() => this.navigateToScreen('SearchingScreen')}
                      style={styles.txtInput}
                    />
                    {
                      this.state.token ?
                        <TouchableOpacity style={styles.searchTouchIcon} onPress={() => this.navigateToScreen('SearchingScreen', 'Agrodealer', this.state.categoryId, this.state.agroName, this.state.subcategoryId)}>
                          <Form name='search1' size={22} color='white' style={styles.searchIcon} />
                        </TouchableOpacity>
                        : null
                    }

                  </View>
                  {
                    this.state.agriSections === false ?
                      <View style={styles.IndicatorSectionCon}>
                        <ActivityIndicator color='red' size='small' animating={true} />
                      </View>
                      :
                      <SelectPicker
                        onValueChange={(value) => { this.loadCategorySections(value) }}
                        selected={this.state.enterpriceName}
                        placeholder='Enterprise'
                        placeholderStyle={{ fontSize: 15, color: 'white' }}
                        style={[styles.txtInput, { marginBottom: height(1) }]}
                        onSelectedStyle={{ color: 'white', width: width(70) }}
                        doneButtonText='Select'
                      >
                        {
                          this.state.agriSections.map((item, key) => {
                            return (
                              <SelectPicker.Item label={item.name} value={item} />
                            )
                          })
                        }

                      </SelectPicker>
                  }

                  {

                    this.state.loadCat ?
                      <View style={styles.IndicatorSectionCon}>
                        <ActivityIndicator color='red' size='small' animating={true} />
                      </View>
                      :

                      <SelectPicker
                        onValueChange={(item) => {
                          // Do anything you want with the value. 
                          // For example, save in state.
                          if (item == null || item == undefined) {
                            this.setState({
                              enterCategory: 'Category',
                              subcategoryId: null
                            })
                            this.navigateToScreen('SearchingScreen', 'Agrodealer', this.state.categoryId, this.state.agroName, this.state.subcategoryId)
                          } else {
                            this.setState({
                              enterCategory: item.name,
                              subcategoryId: item.id
                            })
                            this.navigateToScreen('SearchingScreen', 'Agrodealer', this.state.categoryId, this.state.agroName, this.state.subcategoryId)
                          }
                        }}
                        selected={this.state.enterCategory}
                        placeholder='Category'
                        disabled={this.state.agriCategoryStatus}
                        placeholderStyle={{ fontSize: 15, color: 'white' }}
                        style={styles.txtInput}
                        onSelectedStyle={{ color: 'white', width: width(70) }}
                        doneButtonText='Select'
                      >
                        {
                          this.state.agriCategory.map((item, key) => {
                            return (
                              <SelectPicker.Item label={item.name} value={item} />
                            )
                          })
                        }

                      </SelectPicker>
                  }
                </View>
                {
                  <View style={{ width: width(100), backgroundColor: 'transparent', alignItems: 'center', marginTop: height(0.5), height: height(40) }}>
                    <View style={styles.flatlistCon}>
                      <FlatList
                        data={home_events}
                        // contentContainerStyle={{ height: height(15), alignSelf: 'flex-end',backgroundColor:'red' }}
                        renderItem={({ item, key }) =>
                          <View>
                            <TouchableOpacity key={key} style={styles.flatlistChild}
                              onPress={() => {
                                if (this.state.token) {
                                  this.navigateToScreen('SearchingScreen', item.event_title, item.id, '', item.enterprise)
                                }
                              }}
                            >
                              <Animatable.View
                                duration={2000}
                                animation="zoomIn"
                                iterationCount={1}
                                direction="alternate">
                                <Image style={{ height: height(17), width: width(26) }} source={item.image} />
                                {/* style={{ height: height(17), width: width(26) }} */}
                              </Animatable.View>
                            </TouchableOpacity>
                            <Text style={[styles.childTxt, { fontWeight: 'bold' }]}>{item.event_title}</Text>
                          </View>

                        }
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      // keyExtractor={item => item.email}
                      />
                    </View>
                    {
                      this.state.token ?
                        <View style={{ flex: 1, width: width(100) }}></View>
                        :
                        <View style={{ flex: 1, width: width(100) }}>
                          <Text style={[styles.limitedTxt, { fontWeight: 'bold' }]}>
                            This is a limited view, to access full features, please register.
                          </Text>
                        </View>
                    }
                  </View>
                }
              </View>
            }

          </ScrollView>
          {
            this.state.token ?
              <TouchableOpacity style={[styles.exploreBtn, { backgroundColor: 'rgba(0,153,0,0.6)' }]} onPress={() => this.navigateToScreen('SearchingScreen', 'Find', 'Find By Location')}>
                <Image source={require('../../images/search_white.png')} style={styles.btnIcon} />
                <Text style={styles.explorebtnTxt}>Find by location</Text>
              </TouchableOpacity>
              : null
          }
        </View>
      </SafeAreaView>
    );
  }
}
