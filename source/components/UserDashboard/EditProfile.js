import React, { Component } from 'react';
import { 
  Platform, Text, View, Image, TouchableOpacity,
  ScrollView, TextInput, ActivityIndicator 
} from 'react-native';
import Modal from "react-native-modal";
import { width, height, totalSize } from 'react-native-dimension';
import ImagePicker from 'react-native-image-crop-picker';
import * as Progress from 'react-native-progress';
import Toast from 'react-native-simple-toast';
import { withNavigation } from 'react-navigation';
import { COLOR_PRIMARY, COLOR_GRAY, COLOR_SECONDARY } from '../../../styles/common';
import styles from '../../../styles/UserDashboardStyles/EditProfileStyleSheet';
import UpperView from './UpperView';
import LocalDB from '../../LocalDB/LocalDB';
import { Icon } from 'native-base';



let _this = null;
class EditProfile extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isPassChange: false,
      loading: false,
      newPass: '',
      confirmPass: '',
      name: '',
      email: '',
      phone: '',
      location: '',
      timezone: '',
      about: '',
      fbURL: '',
      twURL: '',
      inURL: '',
      youtubeURL: '',
      instaURL: '',
      hours_type_12: false,
      hours_type_24: false,
      my_hours_type: '',
      modalVisible: false,
      is_picker: false,
      progress: 0,
      avatarSource: null,
      image: null,
      pickerImage: false,
      data: ''
    }
  
  }
  static navigationOptions = { header: null };

  componentDidMount = async () => {
    await this.getUserDetails()

  }


  getUserDetails = async () => {
    token = await LocalDB.getUserProfile()

    this.setState({ loading: true })

    //API calling
    var result;

    const response = await fetch('http://209.97.136.165:8600/api/auth/getuser', {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.token
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client 
    });

    result = await response.json();

    console.log(result)

    if (result.status == 200) {
      this.setState({
        loading: false,
        data: result.data,
      })
    } else {
      this.setState({ loading: false })
      Toast.show('Kindly try again later', Toast.BOTTOM, Toast.LONG)
    }
  }


  imagePicker = () => {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: true,
      // includeExif: true,
      // forceJpg: true,
      compressImageQuality: 0.5
    }).then(async (image) => {
      var fileName = image.path.substring(image.path.lastIndexOf('/') + 1, image.path.length);
      await this.setState({
        avatarSource: { uri: image.path, type: image.mime, name: Platform.OS === 'ios' ? image.filename : fileName },
        image: { uri: image.path, width: image.width, height: image.height, mime: image.mime },
        pickerImage: true
      })
    }).catch((error) => {
      console.log('error:', error);
    });
  }


  render() {


    if (this.state.data == '') {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size='large' color='red' animating={true} />
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <ScrollView>
            <UpperView status={true} image={'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80'} pickerImage={this.state.pickerImage} _imagePicker={this.imagePicker} />
            <View style={styles.titleCon}>
              <View style={{ width: width(61), alignItems: 'flex-start' }}>
                <Text style={styles.titleTxt}>Edit Profile</Text>
              </View>
            </View>
            <View style={styles.textInputCon}>
              <Text style={styles.textInputLabel}>Name<Text style={{ color: 'red' }}>*</Text></Text>
              <TextInput
                onChangeText={(value) => this.setState({ name: value })}
                placeholder='Enter Your Name'
                placeholderTextColor='black'
                value={this.state.data !== "" ? this.state.data.name : null}
                underlineColorAndroid='transparent'
                autoCorrect={false}
                style={styles.textInput}
              />
            </View>
            <View style={styles.textInputCon}>
              <Text style={styles.textInputLabel}>Email<Text style={{ color: 'red' }}>*</Text></Text>
              <TextInput
                onChangeText={(value) => this.setState({ email: value })}
                placeholder='Enter Your Email'
                editable={false}
                placeholderTextColor='black'
                value={this.state.data !== "" ? this.state.data.email : null}
                underlineColorAndroid='transparent'
                autoCorrect={false}
                style={styles.textInput}
              />
            </View>
            <View style={styles.textInputCon}>
              <Text style={styles.textInputLabel}>Password<Text style={{ color: 'red' }}>*</Text></Text>
              <View style={{ flex: 4.1 }}>
              <TextInput
                onChangeText={(value) => this.setState({ password: value })}
                underlineColorAndroid='transparent'
                placeholder='Your Password'
                secureTextEntry={true}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                value={this.state.password}
                autoCorrect={true}
                style={styles.textInput}
                
              />
              </View>
              <View style={{ marginHorizontal: 10 }}>
                <Icon name={this.state.icon} color='black' size={24} onPress={() => this._changeIcon()} />
              </View>
            </View>

            {
              1 ?
                <View style={[styles.profielBtn, { backgroundColor: 'red' }]}>
                  <Text style={{ fontSize: totalSize(1.8), color: COLOR_PRIMARY, marginHorizontal: 0, marginVertical: 3, fontWeight: 'bold' }}>UPDATE PROFILE</Text>
                </View>
                :
                this.state.loading ?
                  <View style={[styles.profielBtn, { backgroundColor: 'red', height: height(7) }]} >
                    <Progress.Circle size={40} indeterminate={false} showsText={true} textStyle={{ fontSize: 10 }} progress={this.state.progress} color={COLOR_PRIMARY} />
                  </View>
                  :
                  <TouchableOpacity style={[styles.profielBtn, { backgroundColor: 'red' }]} onPress={() => this.editProfile()}>
                    <Text style={styles.profielBtnTxt}>UPDATE PROFILE</Text>
                  </TouchableOpacity>
            }
          </ScrollView>
          <Modal
            animationInTiming={500}
            animationIn="slideInLeft"
            animationOut="slideOutRight"
            avoidKeyboard={true}
            // transparent={false}
            isVisible={this.state.modalVisible}
            onBackdropPress={() => this.setState({ modalVisible: false })}
            style={{ flex: 1 }}>
            {
              this.state.isPassChange ?
                <ActivityIndicator color='red' animating={true} size='large' />
                :
                <View style={{ height: height(33), width: width(90), alignSelf: 'center', backgroundColor: COLOR_PRIMARY }}>
                  <View style={{ height: height(4), alignItems: 'flex-end' }}>
                    <TouchableOpacity style={{ elevation: 3, height: height(3.5), width: width(6), justifyContent: 'center', alignItems: 'center', backgroundColor: 'red' }} onPress={() => { this.setState({ modalVisible: false }) }}>
                      <Image source={require('../../images/clear-button.png')} style={{ height: height(2), width: width(3), resizeMode: 'contain' }} />
                    </TouchableOpacity>
                  </View>
                  <Text style={{ fontSize: totalSize(1.8), color: 'black', marginVertical: 10, marginHorizontal: 20, fontWeight: 'bold' }}>Set Your Password</Text>
                  <TextInput
                    onChangeText={(value) => this.setState({ newPass: value })}
                    placeholder='Enter Your New Password'
                    placeholderTextColor={COLOR_GRAY}
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    style={{ height: height(6), marginHorizontal: 20, padding: 10, marginBottom: 10, borderRadius: 5, borderWidth: 0.5, borderColor: COLOR_GRAY, backgroundColor: COLOR_PRIMARY, color: COLOR_SECONDARY, fontSize: totalSize(1.6) }}
                  />
                  <TextInput
                    onChangeText={(value) => this.setState({ confirmPass: value })}
                    placeholder='Confirm New Password'
                    placeholderTextColor={COLOR_GRAY}
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    style={{ height: height(6), marginHorizontal: 20, padding: 10, marginBottom: 10, borderRadius: 5, borderWidth: 0.5, borderColor: COLOR_GRAY, backgroundColor: COLOR_PRIMARY, color: COLOR_SECONDARY, fontSize: totalSize(1.6) }}
                  />
                  <TouchableOpacity style={{ elevation: 3, height: height(6), justifyContent: 'center', alignItems: 'center', borderRadius: 5, marginVertical: 5, marginHorizontal: 20, backgroundColor: 'red' }} onPress={() => { this.postChangePassword() }}>
                    <Text style={{ fontSize: totalSize(1.8), color: COLOR_PRIMARY, fontWeight: 'bold' }}>Change Password</Text>
                  </TouchableOpacity>
                </View>
            }
          </Modal>
        </View>
      );
    }
  }
}
export default withNavigation(EditProfile);

