import React, { Component } from 'react';
import {
    Text, View, Image, TouchableOpacity, ScrollView, Linking, Platform
} from 'react-native';
import Modal from "react-native-modal";
import call from 'react-native-phone-call';
import { width, height, totalSize } from 'react-native-dimension';
import Accordion from 'react-native-collapsible/Accordion';
import { Avatar, Icon } from 'react-native-elements';
import CountDown from 'react-native-countdown-component';
import ImageViewer from 'react-native-image-zoom-viewer';
import HTMLView from 'react-native-htmlview';
import * as Animatable from 'react-native-animatable';
import { COLOR_PRIMARY, COLOR_ORANGE, COLOR_GRAY, COLOR_SECONDARY, S14 } from '../../../styles/common';
import { observer } from 'mobx-react';
// import Store from '../../Stores';
// import store from '../../Stores/orderStore';
import Claim from '../FeatureDetail/Claim'
import Report from '../FeatureDetail/Report'
import styles from '../../../styles/DescriptionStyleSheet';
import FeatureDetail from '../FeatureDetail/FeatureDetail';
import { withNavigation } from 'react-navigation';
const SECTIONS = [
    {
        title: 'First',
        content: [{ name: "Hotels" }, { name: "Hotels" }, { name: "Hotels" }, { name: "Hotels" }, { name: "Hotels" }],
    },
];



const images = [
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    },
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    },
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    },
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    },
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    },
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    },
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    },
    {
        uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
    }
]

const home_listings = [
    {
        listing_id: 1,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 2,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 3,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 4,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 5,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 6,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 7,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
];


class Privacy extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            getCoupon: false,
            reportModel: false,
            modalVisible: false,
            isClaimVisible: false,
            images: [],
            index: 0,
            timer: 0,
            web: false,
        }
    }
    static navigationOptions = {
        header: null
    };
    componentWillMount = async () => {
        // let { orderStore } = Store;
        // let data = orderStore.home.FEATURE_DETAIL.data.listing_detial;
        // // CountDown func call
        // await this.countDown(data.coupon_details.expiry_date)
        // if (data.has_gallery) {
        //   for (let i = 0; i < data.gallery_images.length; i++) {
        //     await this.state.images.push({ url: data.gallery_images[i].url })
        //   }
        // }
        // console.log('iamges=', this.state.images);
    }
    call = (number) => {
        const args = {
            number: number, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }

        call(args).catch(console.error)
    }
    webSite = (url) => {
        if (url !== "") {
            Linking.openURL(url);
        }
    }
    setModalVisible = (state, prop) => {
        if (state === 'claim' && prop === false) {
            this.setState({ reportModel: false, isClaimVisible: true })
        } else {
            if (state === 'report') {
                this.setState({ reportModel: true, isClaimVisible: false })
            }
        }
    }
    hideModels = (state, hide) => {
        if (state === 'claim') {
            this.setState({ isClaimVisible: hide, reportModel: hide })
        } else {
            if (state === 'report') {
                this.setState({ reportModel: hide, reportModel: hide })
            }
        }
    }
    getDirection = async () => {
        data = store.home.FEATURE_DETAIL.data.listing_detial;
        if (Platform.OS === 'android') {
            var url = "geo:" + data.location.latt + "," + data.location.long;
        } else {
            var url = "http://maps.apple.com/?ll=" + data.location.latt + "," + data.location.long;
        }
        Linking.canOpenURL(url)
            .then((supported) => {
                if (!supported) {
                    console.log("Can't handle url: " + url);
                } else {
                    return Linking.openURL(url);
                }
            })
            .catch((err) => console.error('An error occurred', err));
    }
    countDown(eventDate) {
        var eventDate = new Date(eventDate);
        var currentDate = new Date();
        var differenceTravel = eventDate.getTime() - currentDate.getTime();
        var seconds = Math.floor((differenceTravel) / (1000));
        this.setState({ timer: seconds })
    }
    _renderHeader = (section, isActive) => {
        let { orderStore } = Store;
        let data = orderStore.home.FEATURE_DETAIL.data.listing_detial;
        return (
            <View style={[styles.labelCon, { borderBottomWidth: 0 }]}>
                <Animatable.View
                    duration={1000}
                    animation="tada"
                    easing="ease-out"
                    iterationCount={'infinite'}
                    direction="alternate">
                    <Image source={require('../../images/clock.png')} style={styles.labelIcon} />
                </Animatable.View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
                    <Text style={[styles.timeTxt, { color: data.color_code }]}>{data.business_hours_status}</Text>
                </View>
                <Image source={(isActive ? require('../../images/dropDown.png') : require('../../images/next.png'))} style={styles.dropDownIcon} />
            </View>
        );
    }
    _renderContent = (section, content) => {
        let { orderStore } = Store;
        let data = orderStore.home.FEATURE_DETAIL.data.listing_detial;
        return (
            <View style={styles.renderCon}>
                {
                    data.listing_hours.map((item, key) => {
                        return (
                            <View key={key} style={styles.renderStrip}>
                                <View style={styles.stripDay}>
                                    {item.current_day !== true ? <Text style={styles.renderDayTxt}>{item.day_name}</Text> : <Text style={[styles.renderDayTxt, { color: '#32cb55', fontWeight: 'bold' }]}>{item.day_name}</Text>}
                                </View>
                                <View style={styles.stripTime}>
                                    {item.current_day !== true ? <Text style={styles.renderTimeTxt}>{item.is_closed === true ? "closed" : item.timingz}</Text> : <Text style={[styles.renderTimeTxt, { color: '#32cb55', fontWeight: 'bold' }]}>{item.is_closed === true ? "closed" : item.timingz}</Text>}
                                </View>
                            </View>
                        );
                    })
                }
            </View>
        );
    }
    render() {

        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <FeatureDetail callModel={this.setModalVisible} />
                    <Text style={styles.titleTxt}>AIK APP</Text>
                    <View style={{ width: width(88), alignSelf: 'center', marginHorizontal: 15, marginBottom: 10, justifyContent: 'center' }} >
                        <HTMLView
                            value={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dignissim eleifend nisl, eget hendrerit eros iaculis at. Integer at ultrices massa. Morbi lobortis, diam sit amet consequat imperdiet, diam mauris finibus arcu, quis porta turpis justo ac lorem. Sed posuere leo id ultricies gravida. Vivamus id egestas arcu.'}
                            stylesheet={styles.longTxt}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default withNavigation(Privacy)
