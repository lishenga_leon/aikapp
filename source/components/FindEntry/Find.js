import React, { Component } from 'react';
import {
    Text, View, I18nManager,
    ScrollView, RefreshControl, ActivityIndicator
} from 'react-native';
import Modal from "react-native-modal";
import { width, height, totalSize } from 'react-native-dimension';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { COLOR_SECONDARY } from '../../../styles/common';
import Claim from '../FeatureDetail/Claim'
import Report from '../FeatureDetail/Report'
import styles from '../../../styles/FindStyleSheet';
import SelectPicker from 'react-native-form-select-picker';
import ListingComponent from '../Home/ListingComponent';
import LocalDB from '../../LocalDB/LocalDB';
import Toast from 'react-native-simple-toast'
import { withNavigation } from 'react-navigation';
import Geocoder from 'react-native-geocoding';
import Form from 'react-native-vector-icons/AntDesign';


counties = [
    "Mombasa",
    "Kwale",
    "Kilifi",
    "Tana River",
    "Lamu",
    "Taita Taveta",
    "Garissa",
    "Wajir",
    "Mandera",
    "Marsabit",
    "Isiolo",
    "Meru",
    "Tharaka-Nithi",
    "Embu",
    "Kitui",
    "Machakos",
    "Makueni",
    "Nyandarua",
    "Nyeri",
    "Kirinyaga",
    "Muranga",
    "Kiambu",
    "Turkana",
    "West Pokot",
    "Samburu",
    "Trans-Nzoia",
    "Uasin Gishu",
    "Elgeyo-Marakwet",
    "Nandi",
    "Baringo",
    "Laikipia",
    "Nakuru",
    "Narok",
    "Kajiado",
    "Kericho",
    "Bomet",
    "Kakamega",
    "Vihiga",
    "Bungoma",
    "Busia",
    "Siaya",
    "Kisumu",
    "Homa Bay",
    "Migori",
    "Kisii",
    "Nyamira",
    "Nairobi",
]


class Find extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            reportModel: false,
            isClaimVisible: false,
            county: '',
            loading: false,
            data: '',
            next_page_url: '',
            per_page: '',
            param: {},
            reCaller: false,
            loadMore: false,
            region: {
                latitude: 1.2921,
                longitude: 36.8219,
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
            },
            coord: ''
        }
        I18nManager.forceRTL(false);
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: navigation.state.params.list_title,
        headerTintColor: 'white',
        headerTitleStyle: {
            fontSize: totalSize(2),
            fontWeight: 'normal'
        },
        headerStyle: {
            backgroundColor: '#009900'
        }
    });


    componentDidMount = async () => {
        Geocoder.init("AIzaSyAWLQgl-Pv6qz5yrQ6YpMe8Zqkyzebh6cs");
        await this.getSearchList()
        var coord = []
        if (this.state.data !== '') {
            this.state.data.forEach((item) => {
                if (item.street_address !== null || item.street_address !== 'NULL') {
                    coord.push(this.getCoordinates(item))
                }
            })
            const items = await Promise.all(coord)
            this.setState({
                coord: items
            })
        }

    }


    getSearchList = async () => {

        token = await LocalDB.getUserProfile()

        this.setState({ loading: true })

        var result, params;

        params = {
            county: 47
        }

        const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.token
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client 
            body: JSON.stringify(params)// body data type must match "Content-Type" header
        });

        result = await response.json();

        if (result.status == 200) {

            this.setState({
                loading: false,
                data: result.data.data,
                next_page_url: result.data.next_page_url,
                per_page: result.data.total,
                param: params
            })
        } else {
            Toast.show('Kindly try again later', Toast.LONG, Toast.TOP)
            this.setState({ loading: false })
        }
    }

    searchCounty = async (county) => {
        token = await LocalDB.getUserProfile()
        Geocoder.init("AIzaSyAWLQgl-Pv6qz5yrQ6YpMe8Zqkyzebh6cs");
        name = county - 1
        coord = await Geocoder.from(counties[name] + 'kenya')

        var location = coord.results[0].geometry.location;
        this.setState({
            region: {
                latitude: location.lat,
                longitude: location.lng,
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
            },
            loading: true, county: county
        })

        var result, params;
        params = {
            county: county
        }

        const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.token
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client 
            body: JSON.stringify(params)// body data type must match "Content-Type" header
        });

        result = await response.json();

        if (result.status == 200) {

            var coord = []
            result.data.data.forEach((item) => {
                if (item.street_address !== null || item.street_address !== 'NULL') {
                    coord.push(this.getCoordinates(item))
                }
            })
            const items = await Promise.all(coord)
            if (result.data.to >= 1) {

                this.setState({
                    loading: false,
                    data: result.data.data,
                    next_page_url: result.data.next_page_url,
                    per_page: result.data.total,
                    coord: items,
                    param: params
                })
            } else {

                this.setState({
                    loading: false,
                    data: result.data.data,
                    next_page_url: result.data.next_page_url,
                    per_page: 0,
                    coord: items,
                    param: params
                })
            }

        } else {
            Toast.show('Kindly try again later', Toast.LONG, Toast.TOP)
            this.setState({ loading: false })
        }
    }

    loadMore = async (pageNo) => {

        if (pageNo !== null) {

            this.setState({ loadmore: true })

            token = await LocalDB.getUserProfile()

            const response = await fetch(pageNo, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(this.state.param)// body data type must match "Content-Type" header
            });

            result = await response.json();

            if (result.status == 200) {
                var next_page_url, per_page, finish;
                result.data.data.forEach((item) => {
                    finish = this.state.data.push(item)
                    next_page_url = result.data.next_page_url,
                        per_page = result.data.total

                })
                var coord = []
                this.state.data.forEach((item) => {
                    if (item.street_address !== null || item.street_address !== 'NULL') {
                        coord.push(this.getCoordinates(item))
                    }
                })
                const items = await Promise.all(coord)
                this.setState({
                    coord: items,
                    next_page_url: next_page_url,
                    per_page: per_page,
                    loadmore: false
                })
            } else {
                this.setState({ loadmore: false })
                Toast.show('Kindly try again later', Toast.TOP, Toast.LONG)
            }
            this.setState({ reCaller: false })
        } else {

            Toast.show('End of List', Toast.TOP, Toast.LONG)
            this.setState({ reCaller: false })

            return
        }
    }


    isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    static navigationOptions = {
        header: null,
    };


    getCoordinates = async (item) => {

        var result, location, coord;
        Geocoder.init("AIzaSyAWLQgl-Pv6qz5yrQ6YpMe8Zqkyzebh6cs");

        try {
            coord = await Geocoder.from(item.street_address + 'kenya')
            var location = coord.results[0].geometry.location;
            result = {
                company_name: item.company_name,
                location: {
                    latitude: location.lat,
                    longitude: location.lng,
                    latitudeDelta: 0.00922 * 1.5,
                    longitudeDelta: 0.00421 * 1.5
                }
            }
            return result
        } catch (error) {
            result = {
                company_name: item.company_name,
                location: {
                    latitude: 1.2921,
                    longitude: 36.8219,
                    latitudeDelta: 0.00922 * 1.5,
                    longitudeDelta: 0.00421 * 1.5
                }
            }
            return result

        }
    }



    render() {

        // if(this.state.coord !== '' ){
        //     this.state.coord.map(marker => (
        //         console.log(marker.location)
        //     ))
        // }

        return (
            <View style={styles.container}>
                <View style={styles.mapCon}>
                    <MapView
                        ref={(ref) => this.mapView = ref}
                        showsScale={true}
                        rotateEnabled={true}
                        zoomEnabled={true}
                        zoomControlEnabled={true}
                        toolbarEnabled={false}
                        showsCompass={true}
                        showsBuildings={true}
                        showsIndoors={true}
                        provider={PROVIDER_GOOGLE}
                        showsMyLocationButton={true}
                        minZoomLevel={1}
                        maxZoomLevel={6}
                        mapType={"standard"}
                        animateToRegion={{
                            latitude: 1.2921,
                            longitude: 36.8219,
                            latitudeDelta: 0.00922 * 1.5,
                            longitudeDelta: 0.00421 * 1.5
                        }}
                        style={styles.map}
                        region={this.state.region}
                    // onRegionChange={this.onRegionChange.bind(this)}
                    >
                        {
                            1 ?
                                <MapView.Marker
                                    coordinate={this.state.region}
                                    // title={'Current location'}
                                    // description={'I am here'}
                                    pinColor={'#3edc6d'}
                                />
                                : <View></View>
                        }
                        {
                            this.state.coord !== '' ?
                                this.state.coord.map(marker => (
                                    <MapView.Marker
                                        coordinate={marker.location}
                                        title={marker.company_name}
                                        onPress={e => this.marker(e.nativeEvent)}
                                        pinColor={'#3edc6d'}
                                    />
                                    // <View>{console.log(this.getCoordinates(marker.street_address))}</View>
                                ))
                                : <View></View>
                        }

                    </MapView>
                </View>
                <View style={styles.searchCon}>
                    <SelectPicker
                        onValueChange={(value) => {
                            // Do anything you want with the value. 
                            // For example, save in state.
                            this.searchCounty(value)
                        }}
                        selected={this.state.county}
                        placeholder='County'
                        placeholderStyle={{ fontSize: 15, color: 'white' }}
                        style={styles.txtInput}
                        onSelectedStyle={{ color: 'white' }}
                        doneButtonText='Select'
                    >
                        <SelectPicker.Item label="Mombasa" value="1" />
                        <SelectPicker.Item label="Kwale" value="2" />
                        <SelectPicker.Item label="Kilifi" value="3" />
                        <SelectPicker.Item label="Tana River" value="4" />
                        <SelectPicker.Item label="Lamu" value="5" />
                        <SelectPicker.Item label="Taita Taveta" value="6" />
                        <SelectPicker.Item label="Garissa" value="7" />
                        <SelectPicker.Item label="Wajir" value="8" />
                        <SelectPicker.Item label="Mandera" value="9" />
                        <SelectPicker.Item label="Marsabit" value="10" />
                        <SelectPicker.Item label="Isiolo" value="11" />
                        <SelectPicker.Item label="Meru" value="12" />
                        <SelectPicker.Item label="Tharaka-Nithi" value="13" />
                        <SelectPicker.Item label="Embu" value="14" />
                        <SelectPicker.Item label="Kitui" value="15" />
                        <SelectPicker.Item label="Machakos" value="16" />
                        <SelectPicker.Item label="Makueni" value="17" />
                        <SelectPicker.Item label="Nyandarua" value="18" />
                        <SelectPicker.Item label="Nyeri" value="19" />
                        <SelectPicker.Item label="Kirinyaga" value="20" />
                        <SelectPicker.Item label="Muranga" value="21" />
                        <SelectPicker.Item label="Kiambu" value="22" />
                        <SelectPicker.Item label="Turkana" value="23" />
                        <SelectPicker.Item label="West Pokot" value="24" />
                        <SelectPicker.Item label="Samburu" value="25" />
                        <SelectPicker.Item label="Trans-Nzoia" value="26" />
                        <SelectPicker.Item label="Uasin Gishu" value="27" />
                        <SelectPicker.Item label="Elgeyo-Marakwet" value="28" />
                        <SelectPicker.Item label="Nandi" value="29" />
                        <SelectPicker.Item label="Baringo" value="30" />
                        <SelectPicker.Item label="Laikipia" value="31" />
                        <SelectPicker.Item label="Nakuru" value="32" />
                        <SelectPicker.Item label="Narok" value="33" />
                        <SelectPicker.Item label="Kajiado" value="34" />
                        <SelectPicker.Item label="Kericho" value="35" />
                        <SelectPicker.Item label="Bomet" value="36" />
                        <SelectPicker.Item label="Kakamega" value="37" />
                        <SelectPicker.Item label="Vihiga" value="38" />
                        <SelectPicker.Item label="Bungoma" value="39" />
                        <SelectPicker.Item label="Busia" value="40" />
                        <SelectPicker.Item label="Siaya" value="41" />
                        <SelectPicker.Item label="Kisumu" value="42" />
                        <SelectPicker.Item label="Homa Bay" value="43" />
                        <SelectPicker.Item label="Migori" value="44" />
                        <SelectPicker.Item label="Kisii" value="45" />
                        <SelectPicker.Item label="Nyamira" value="46" />
                        <SelectPicker.Item label="Nairobi" value="47" />

                    </SelectPicker>
                    {
                        <View style={styles.searchTouchIcon}>
                            <Form name='search1' size={22} color='white' style={styles.searchIcon} />
                        </View>
                    }

                </View>
                {
                    this.state.loading ?
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <ActivityIndicator size='large' color='red' animating={true} />
                        </View>
                        :
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            onScroll={({ nativeEvent }) => {
                                if (this.isCloseToBottom(nativeEvent)) {
                                    if (this.state.reCaller === false) {
                                        this.loadMore(this.state.next_page_url);
                                    }
                                    this.setState({ reCaller: true })
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    colors={['white']}
                                    progressBackgroundColor='red'
                                    tintColor='red'
                                    refreshing={this.state.loading}
                                    onRefresh={() => this.getSearchList()}
                                />
                            }
                            scrollEventThrottle={400}
                        >

                            <View style={{ height: height(6), width: width(100), backgroundColor: 'white', justifyContent: 'center', marginBottom: 5, alignItems: 'flex-start' }}>
                                <Text style={{ fontSize: totalSize(2.2), color: COLOR_SECONDARY, marginHorizontal: 15, marginVertical: 10 }}>{this.state.per_page} results found</Text>
                            </View>
                            {
                                this.state.data !== "" ?
                                    this.state.data.map((item, key) => {
                                        return (
                                            <ListingComponent item={item} key={key} listStatus={true} />
                                        )
                                    })
                                    : null
                            }
                            {
                                this.state.loadmore ?
                                    <View style={{ height: height(7), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
                                        {
                                            this.state.loadmore ?
                                                <ActivityIndicator size='large' color='red' animating={true} />
                                                : null
                                        }
                                    </View>
                                    :
                                    null
                            }
                        </ScrollView>
                }
                <Modal
                    animationInTiming={500}
                    animationIn="slideInLeft"
                    animationOut="slideOutRight"
                    avoidKeyboard={true}
                    // transparent={false}
                    isVisible={this.state.reportModel}
                    onBackdropPress={() => this.setState({ reportModel: false })}
                    style={{ flex: 1 }}>
                    <Report hideModels={this.hideModels} />
                </Modal>
                <Modal
                    animationInTiming={500}
                    animationIn="slideInLeft"
                    animationOut="slideOutRight"
                    avoidKeyboard={true}
                    // transparent={false}
                    isVisible={this.state.isClaimVisible}
                    onBackdropPress={() => this.setState({ isClaimVisible: false })}
                    style={{ flex: 1 }}>
                    <Claim hideModels={this.hideModels} />
                </Modal>
            </View>
        );
    }
}

export default withNavigation(Find)
