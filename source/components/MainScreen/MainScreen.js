import React, { Component, useEffect } from 'react';
import { Text, View, Image, ImageBackground, TouchableOpacity } from 'react-native';
import styles from '../../../styles/MainScreenStyle'
import Geocoder from 'react-native-geocoding';
import { RemotePushController } from '../../RemotePushController'
import PushNotification from 'react-native-push-notification'



export default class MainScreen extends Component<Props> {
  constructor(props) {
    // let { orderStore } = Store;
    // let data = orderStore.settings.data;
    super(props);
  }
  static navigationOptions = { header: null };

  componentDidMount = async () => {
    Geocoder.init("AIzaSyAWLQgl-Pv6qz5yrQ6YpMe8Zqkyzebh6cs");
  }

  render() {

    // let { orderStore } = Store;
    // let data = orderStore.settings.data;
    return (
      <View style={styles.container}>
        {/* <ImageBackground source={require('../../images/bk_ground.jpg')} style={styles.imgCon}> */}
        {/* <ImageBackground source={require('../../images/Downtown_Shadownew.png')} style={styles.imgCon}> */}
        <View style={styles.logoView}>
          <Image source={require('../../images/images/AIK-App-Logo.png')} style={styles.logoImg} />
          {/* <Text style={styles.logoTxt}>AIK</Text> */}
        </View>
        <View style={styles.buttonView}>
          <TouchableOpacity style={[styles.signInBtn, { backgroundColor: 'rgba(0,0,0,0.8)' }]} onPress={() => { this.props.navigation.navigate('SignIn') }}>
            <Text style={styles.signTxt}>Sign In</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.signUpBtn, { backgroundColor: 'rgba(0,153,0,0.9)' }]} onPress={() => { this.props.navigation.navigate('SignUp') }}>
            <Text style={styles.signUpTxt}>Sign Up</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.expTxt} onPress={() => this.props.navigation.navigate('Drawer')}>Explore.</Text>
        {/* </ImageBackground> */}
        {/* </ImageBackground> */}
      </View>
    );

    // let { orderStore } = Store;
    // let data = orderStore.settings.data;
    // return (
    //   <View style={styles.container}>
    //     <ImageBackground source={require('../../images/bk_ground.jpg')} style={styles.imgCon}>
    //       <ImageBackground source={require('../../images/Downtown_Shadownew.png')} style={styles.imgCon}>
    //         <View style={styles.logoView}>
    //           <Image source={{ uri: data.logo }} style={styles.logoImg} />
    //           <Text style={styles.logoTxt}>{data.slogan}</Text>
    //         </View>
    //         <View style={styles.buttonView}>
    //           <TouchableOpacity style={styles.signInBtn} onPress={() => { this.props.navigation.navigate('SignIn') }}>
    //             <Text style={styles.signTxt}>{data.main_screen.sign_in}</Text>
    //           </TouchableOpacity>
    //           <TouchableOpacity style={[styles.signUpBtn, { backgroundColor: orderStore.settings.data.main_clr }]} onPress={() => { this.props.navigation.navigate('SignUp') }}>
    //             <Text style={styles.signUpTxt}>{data.main_screen.sign_up}</Text>
    //           </TouchableOpacity>
    //         </View>
    //         <Text style={styles.expTxt} onPress={() => this.props.navigation.navigate('Drawer')}>{data.main_screen.explore}</Text>
    //       </ImageBackground>
    //     </ImageBackground>
    //   </View>
    // );
  }
}
