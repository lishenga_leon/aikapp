import React, { Component } from 'react';
import { Text, ActivityIndicator, View, Dimensions, ScrollView } from 'react-native';
// import store from '../../Stores/orderStore';
import { height, width, totalSize } from 'react-native-dimension';
import { COLOR_PRIMARY, COLOR_SECONDARY } from '../../../styles/common';
import LocalDB from '../../LocalDB/LocalDB';
import EventListComponent from './EventListComponent';


class EventsTabs extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      index: 0,
      data: '',
      next_page_url: '',
      per_page: '',
      loadMore: false
    }
  }
  static navigationOptions = { header: null };

  componentDidMount = async () => {
    await this.getEvents()
  }

  getEvents = async () => {

    this.setState({ loading: true })

    //API calling
    var result;
    const params = {
      a: 10
    }

    const response = await fetch('http://209.97.136.165:8600/api/events/list/events', {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' 
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client 
      // body: JSON.stringify(params)// body data type must match "Content-Type" header
    });

    result = await response.json();

    console.log(result)

    // alert('leon')
    // // orderStore.SEARCHING.LISTING_SEARCH = response;
    if (result.status == 200) {
      // console.log('listSeacrh===>', store.SEARCHING.LISTING_SEARCH);
      this.setState({
        loading: false,
        data: result.data.data,
        next_page_url: result.data.next_page_url,
        per_page: result.data.per_page
      })
    } else {
      // store.SEARCHING.LISTING_SEARCH.data = [];
      this.setState({ loading: false })
    }
  }

  _renderLabel = ({ route }) => (
    <Text style={{ fontSize: 14, color: COLOR_SECONDARY }}>{route.title}</Text>
  );


  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };


  loadMore = async (pageNo) => {
    this.setState({ loadmore: true })

    token = await LocalDB.getUserProfile()

    console.log(pageNo)
    // console.log(options)
    // let res = await axios.post(pageNo, { a: 10 }, options)
    // console.log(res)


    const params = {
      a: 10
    }

    const response = await fetch(pageNo, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.token
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client 
      body: JSON.stringify(params)// body data type must match "Content-Type" header
    });

    result = await response.json();

    if (result.status == 200) {
      //forEach Loop LoadMore results
      // response.data.listings.forEach((item) => {
      //     data.listings.push(item);
      // })
      var finish, next_page_url, per_page;
      result.data.data.forEach((item) => {
        // console.log(item)
        finish = this.state.data.push(item)
        next_page_url = result.data.next_page_url,
          per_page = this.state.per_page + result.data.per_page

      })
      this.setState({
        // data: finish,
        next_page_url: next_page_url,
        per_page: per_page,
        loadmore: false
      })
      // data.pagination = response.data.pagination;
      // this.setState({ })
    } else {
      this.setState({ loadmore: false })
      // Toast.show(response.data.no_more)
    }
    this.setState({ reCaller: false })
  }


  render() {

    if (this.state.loading == true) {
      return (
        <View style={{ height: height(100), width: width(100), justifyContent: 'center', alignItems: 'center', flex: 1 }}>
          <ActivityIndicator size='large' color='red' animating={true} />
        </View>
      );
    }
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        onScroll={({ nativeEvent }) => {
          if (this.isCloseToBottom(nativeEvent)) {
            if (this.state.reCaller === false) {
              this.loadMore(this.state.next_page_url);
            }
            this.setState({ reCaller: true })
          }
        }}
        scrollEventThrottle={400}>
        <View style={{ height: height(6), width: width(100), backgroundColor: 'white', justifyContent: 'center', marginBottom: 5, alignItems: 'flex-start' }}>
          <Text style={{ fontSize: totalSize(2.2), color: COLOR_SECONDARY, marginHorizontal: 15, marginVertical: 10 }}>{this.state.per_page} results found</Text>
        </View>
        {
          this.state.data !== "" ?
            this.state.data.map((item, key) => {
              return (
                <EventListComponent item={item} key={key} listStatus={true} />
              )
            })
            : null
        }
        {
          0 ?
            <View style={{ height: height(7), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
              {
                this.state.loadmore ?
                  <ActivityIndicator size='large' color='red' animating={true} />
                  : null
              }
            </View>
            :
            null
        }
      </ScrollView>
    )
    //   return (
    //     <TabView
    //       navigationState={{
    //         index: this.state.index,
    //         routes: [
    //           // { key: 'create', title: 'Create Events' },
    //           { key: 'publish', title: 'Events' },
    //         ],
    //       }}
    //       renderScene={({ route, jumpTo }) => {
    //         switch (route.key) {
    //           // case 'create':
    //           //   return <CreateEvent jumpTo={jumpTo} />;
    //           case 'publish':
    //             return <PublishedEvents jumpTo={jumpTo} />;
    //           default:
    //             return null;
    //         }
    //       }}
    //       // renderScene={SceneMap({
    //       //   create: CreateEvent,
    //       //   publish: PublishedEvents,
    //       //   pending: PendingEvents,
    //       //   expired: ExpiredEvents
    //       // })}
    //       initialLayout={{
    //         width: Dimensions.get('window').width,
    //         height: Dimensions.get('window').height
    //       }}
    //       swipeEnabled={true}
    //       animationEnabled={true}
    //       onIndexChange={index => this.setState({ index: index })}
    //       canJumpToTab={true}
    //       tabBarPosition='bottom'
    //       lazy={false}
    //       // renderLazyPlaceholder='loading...'
    //       renderTabBar={props =>
    //         <TabBar
    //           {...props}
    //           renderLabel={this._renderLabel}
    //           scrollEnabled={true}
    //           bounces={true}
    //           useNativeDriver={true}
    //           pressColor={'red'}
    //           style={{ height: 45, justifyContent: 'center', backgroundColor: COLOR_PRIMARY }}
    //           labelStyle={{ color: COLOR_SECONDARY }}
    //           tabStyle={{
    //             width: width(50)
    //           }}
    //           indicatorStyle={{ backgroundColor: 'red', height: 3 }}
    //           activeLabelStyle={{ color: 'red' }}
    //         />
    //       }
    //     />
    //   )
  }
}

// const Tab = TabNavigator({
//   CreateEvent: {
//     screen: CreateEvent,
//     navigationOptions: {
//       title: "Create Event",
//     }
//   },
//   PublishedEvents: {
//     screen: PublishedEvents,
//     navigationOptions: {
//       title: "Published Events",
//     }
//   },
//   PendingEvents: {
//     screen: PendingEvents,
//     navigationOptions: {
//       title: "Pending Events",
//     }
//   },
//   ExpiredEvents: {
//     screen: ExpiredEvents,
//     navigationOptions: {
//       title: "Expired Events",
//     }
//   },
// },
//   {
//     order: ['CreateEvent', 'PublishedEvents', 'PendingEvents', 'ExpiredEvents'],
//     initialRouteName: 'CreateEvent',
//     tabBarPosition: 'bottom',
//     removeClippedSubviews: true,
//     animationEnabled: true,
//     swipeEnabled: true,
//     showIcon: false,
//     animationEnabled: true,
//     lazy: true,
//     backBehavior: true,
//     tabBarOptions: {
//       activeTintColor: COLOR_SECONDARY,
//       inactiveTintColor: COLOR_GRAY,
//       allowFontScaling: true,
//       scrollEnabled: true,
//       showIcon: false,
//       upperCaseLabel: false,
//       pressColor: COLOR_ORANGE,
//       labelStyle: {
//         // fontFamily: FONT_BOLD,
//         fontSize: totalSize(1.6),
//         // textAlign: 'center',
//         justifyContent: 'center',
//         alignItems: 'center',
//         // padding:0
//       },
//       tabStyle: {
//         // height:height(6),
//         // width:width(35)
//         // justifyContent: 'center',
//         // alignItems: 'center',
//       },
//       style: {
//         // height:height(6),
//         // justifyContent:'center',
//         backgroundColor: COLOR_PRIMARY,
//       },
//       indicatorStyle: {
//         borderColor: COLOR_ORANGE,
//         borderWidth: 2,
//       },
//     },
//   }
// );
export default EventsTabs;