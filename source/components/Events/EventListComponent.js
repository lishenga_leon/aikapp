import React, { Component } from 'react';
import { Text, View, ImageBackground, TouchableOpacity } from 'react-native';
import { width } from 'react-native-dimension';
import Icon from 'react-native-vector-icons/AntDesign';
import IconPhone from 'react-native-vector-icons/Ionicons';
import IconLoc from 'react-native-vector-icons/MaterialIcons';
import StarRating from 'react-native-star-rating';
import { COLOR_GRAY, COLOR_ORANGE } from '../../../styles/common';
import { withNavigation } from 'react-navigation';
import styles from '../../../styles/Home';



class EventListComponent extends Component<Props> {
    render() {
        let item = this.props.item;
        let status = this.props.listStatus;
        
        return (
            <TouchableOpacity style={[styles.featuredFLItem,{ width: status? width(95) : width(90) }]} onPress={() => {this.props.navigation.navigate('Descriptions', { data: item })}}>
                <ImageBackground source={{ uri: 'https://www.oyorooms.com/blog/wp-content/uploads/2018/03/fe-30.jpg'}} style={styles.featuredImg}>
                    <TouchableOpacity style={[styles.closedBtn, { backgroundColor: item.color_code }]}>
                    </TouchableOpacity>
                </ImageBackground>
                <View style={styles.txtViewCon}>
                    <View style={{ width: width(50), alignItems:'flex-start' }}>
                        <Text style={styles.txtViewHeading}>{item.title}</Text>
                    </View>
                    <View style={{ marginTop: 2, width: width(45), marginHorizontal: 8, flexDirection: 'row', alignItems: 'center' }}>
                        <IconPhone
                            size={18}
                            name='md-time'
                            type='evilicon'
                            color='#8a8a8a'
                            containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}
                        />
                        <Text style={{ fontSize: 10, color: 'black', fontWeight: 'bold', marginLeft: 5, marginRight: 5 }}>From:</Text>
                        <Text style={{ fontSize: 10, color: 'black', fontWeight: 'bold',}}>{item.event_date}</Text>
                    </View>
                    <View style={{ marginTop: 2, width: width(45), marginHorizontal: 8, flexDirection: 'row', alignItems: 'center' }}>
                        <Icon
                            size={18}
                            name='calendar'
                            type='evilicon'
                            color='#8a8a8a'
                            containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}
                        />
                        <Text style={{ fontSize: 10, color: 'black', fontWeight: 'bold', marginLeft: 5, marginRight: 5 }}>To:</Text>
                        <Text style={{ fontSize: 10,  color: 'black', fontWeight: 'bold', }}>{item.event_date}</Text>
                    </View>
                    <View style={{ marginTop: 2, width: width(45), marginHorizontal: 8, flexDirection: 'row', alignItems: 'center' }}>
                        <IconLoc
                            size={18}
                            name='place'
                            type='evilicon'
                            color='#8a8a8a'
                            containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}
                        />
                        <Text style={{ fontSize: 10, color: 'black', fontWeight: 'bold', marginLeft: 5, marginRight: 5 }}>Venue:</Text>
                        <Text style={{ fontSize: 10,  color: 'black', fontWeight: 'bold', }}>{item.location}</Text>
                    </View>
                    {/* <View style={styles.ratingCon}>
                        <View style={styles.gradingCon}>
                            <StarRating
                                disabled={false}
                                maxStars={5}
                                starSize={13}
                                fullStarColor={COLOR_ORANGE}
                                containerStyle={{ marginHorizontal: 10 }}
                                rating={item.rating_stars === "" ? 0 : item.rating_stars}
                            />
                        </View>
                        <Icon
                            size={20}
                            name='eye'
                            type='evilicon'
                            color='#8a8a8a'
                            containerStyle={{ marginLeft: 0, marginVertical: 3 }}
                        />
                        <Text style={styles.ratingTxt}>{item.total_views}</Text>
                    </View> */}
                </View>
            </TouchableOpacity>
        );
    }
}
export default withNavigation(EventListComponent)
