import React, { Component } from 'react';
import { Platform, Text, View, ActivityIndicator, Image, ImageBackground } from 'react-native';
import { width } from 'react-native-dimension';
import styles from '../../../styles/FeatureDetailStyle';
import { withNavigation } from 'react-navigation';


class FeatureDetails extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }
  static navigationOptions = {
    header: null,
  };
  render() {

    const details = this.props.navigation.getParam('data', 'Descriptions')

    return (
      <View>
        {
          details.title != null ?
            <ImageBackground source={{ uri: 'http://drommonster.co.za/wp-content/uploads/2018/02/Blog2.jpg' }} style={styles.bgImg}>
              <View style={styles.imgConView}>
                <View style={styles.imgSubConView}>
                  <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                    <Text style={styles.title}>{details.title}</Text>
                  </View>
                </View>
              </View>
            </ImageBackground>
            : null
        }
      </View>
    );
  }
}

export default withNavigation(FeatureDetails)