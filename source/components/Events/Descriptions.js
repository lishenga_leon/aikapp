import React, { Component } from 'react';
import {
    Text, View, Image, TouchableOpacity, ScrollView, Linking, Platform
} from 'react-native';
import Modal from "react-native-modal";
import call from 'react-native-phone-call';
import { width, height, totalSize } from 'react-native-dimension';
import Accordion from 'react-native-collapsible/Accordion';
import { Avatar, Icon } from 'react-native-elements';
import CountDown from 'react-native-countdown-component';
import ImageViewer from 'react-native-image-zoom-viewer';
import HTMLView from 'react-native-htmlview';
import * as Animatable from 'react-native-animatable';
import { COLOR_PRIMARY, COLOR_ORANGE, COLOR_GRAY, COLOR_SECONDARY, S14 } from '../../../styles/common';
import IconPhone from 'react-native-vector-icons/MaterialCommunityIcons';
import IconUser from 'react-native-vector-icons/Fontisto';
import IconMoney from 'react-native-vector-icons/FontAwesome';
import IconLock from 'react-native-vector-icons/Entypo';
import styles from '../../../styles/DescriptionStyleSheet';
import FeatureDetails from './FeatureDetails';
import { withNavigation } from 'react-navigation';


class Descriptions extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            getCoupon: false,
            reportModel: false,
            modalVisible: false,
            isClaimVisible: false,
            images: [],
            index: 0,
            timer: 0,
            web: false,
        }
    }

    static navigationOptions = {
        header: null
    };

    render() {

        const details = this.props.navigation.getParam('data', 'Descriptions')
        console.log(details)

        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <FeatureDetails callModel={this.setModalVisible} />
                    {
                        details.entrance_fee != null ?
                            <TouchableOpacity style={styles.labelCon}>
                                <IconMoney name='money' color='black' size={30} style={[styles.labelIcon, { marginTop: height(2) }]} />
                                <View style={styles.labeTxtCon}>
                                    <Text style={styles.labelTxt}>{details.entrance_fee} </Text>
                                </View>
                            </TouchableOpacity>
                            : null
                    }
                    {
                        details.event_date != null ?
                            <TouchableOpacity style={styles.labelCon}>
                                <IconUser name='date' color='black' size={30} style={[styles.labelIcon, { marginTop: height(2) }]} />
                                <View style={styles.labeTxtCon}>
                                    <Text style={styles.labelTxt}>{details.event_date} </Text>
                                </View>
                            </TouchableOpacity>
                            : null
                    }
                    {
                        details.location != null ?
                            <TouchableOpacity style={styles.labelCon}>
                                <Image source={require('../../images/address.png')} style={styles.labelIcon} />
                                <View style={styles.labeTxtCon}>
                                    <Text style={styles.labelTxt}>{details.location} </Text>
                                </View>
                            </TouchableOpacity>
                            : null
                    }
                    <Text style={styles.titleTxt}>Description</Text>
                    <View style={{ width: width(88), alignSelf: 'center', marginHorizontal: 15, marginBottom: 10, justifyContent: 'center' }} >
                        {
                            details.description != null ?
                            <HTMLView
                                value={details.description}
                                stylesheet={styles.longTxt}
                            />
                            : null
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default withNavigation(Descriptions)
