import React, { Component } from 'react';
import {
    Text, View, Image, ImageBackground, TouchableOpacity, I18nManager,
    ScrollView, TextInput, ActivityIndicator
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { width, height, totalSize } from 'react-native-dimension';
import { COLOR_PRIMARY, COLOR_GRAY, COLOR_SECONDARY } from '../../../styles/common';
import Modal from "react-native-modal";
import { CheckBox, Icon } from 'react-native-elements';
import { observer } from 'mobx-react';
// import Store from '../../Stores';
import styles from '../../../styles/Home';
import ApiController from '../../ApiController/ApiController';
// import store from '../../Stores/orderStore';
import Toast from 'react-native-simple-toast'
import ListingComponent from '../Home/ListingComponent';
import LocalDB from '../../LocalDB/LocalDB';
import axios from 'axios';

const home_listings = [
    {
        listing_id: 1,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 2,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 3,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 4,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 5,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 6,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 7,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
];



export default class SearchingScreen extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            reCaller: false,
            loading: false,
            loadmore: false,
            sorting: false,
            sortCheck: false,
            search: '',
            data: '',
            next_page_url: '',
            per_page: '',
            arr: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }]
        }
    }
    static navigationOptions = { header: null };
    navigateToScreen = (route, title) => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.setParams({ otherParam: title });
        this.props.navigation.dispatch(navigateAction);
    }
    _sort = () => this.setState({ sorting: !this.state.sorting })
    componentDidMount = async () => {
        this.setState({ loading: false })
        await this.getSearchList()
    }
    getSearchList = async () => {
        // let { orderStore } = Store;
        // let { params } = this.props.navigation.state;
        // if (this.state.search.length !== 0) {
        //     store.SEARCH_OBJ.by_title = this.state.search;
        // } else {
        //     // store.SEARCH_OBJ.by_title = this.state.search;
        // }
        // if (store.moveToSearch === true) {
        //     store.SEARCH_OBJ.l_category = store.CATEGORY.category_id;
        // }
        // console.log('paramsPPP===>>>', store.SEARCH_OBJ);

        if(this.state.data.length){
            console.log(this.state.data)
            this.setState({
                data: ''
            })
        }

        token = await LocalDB.getUserProfile()

        this.setState({ loading: true })
       
        //API calling
        var result;
        if (this.props.navigation.getParam('otherParam', 'SearchingScreen')) {
            // res = await axios.post('http://209.97.136.165:8600/api/listings/search/listing', { enterprise: this.props.navigation.getParam('otherParam', 'SearchingScreen') }, options)

            const params = {
                enterprise: this.props.navigation.getParam('otherParam', 'SearchingScreen')
            }

            const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(params)// body data type must match "Content-Type" header
            });

            result = await response.json();

        } else if (this.props.navigation.getParam('agroName', 'SearchingScreen')) {
            // res = await axios.post('http://209.97.136.165:8600/api/listings/search/listing', { name: this.props.navigation.getParam('agroName', 'SearchingScreen') }, options)

            const params = {
                name: this.props.navigation.getParam('agroName', 'SearchingScreen')
            }

            const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(params)// body data type must match "Content-Type" header
            });

            result = await response.json();


        } else {
            // res = await axios.post('http://209.97.136.165:8600/api/listings/search/listing', { a: 10 }, options)

            const params = {
                a: 10
            }

            const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(params)// body data type must match "Content-Type" header
            });

            result = await response.json();

        }

        console.log(result)

        // alert('leon')
        // // orderStore.SEARCHING.LISTING_SEARCH = response;
        if (result.status == 200) {
            // console.log('listSeacrh===>', store.SEARCHING.LISTING_SEARCH);
            this.setState({
                loading: false,
                data: result.data.data,
                next_page_url: result.data.next_page_url,
                per_page: result.data.per_page
            })
        } else {
            // store.SEARCHING.LISTING_SEARCH.data = [];
            Toast.show(res.status)
            this.setState({ loading: false })
        }
    }
    resetSearchList = async () => {
        let { orderStore } = Store;
        store.CATEGORY = {};
        store.SEARCH_OBJ = {};
        await this.setState({
            search: ''
        })
        try {
            this.setState({ loading: true })
            //API calling
            let response = await ApiController.post('listing-search');
            orderStore.SEARCHING.LISTING_SEARCH = response;
            if (response.success === true) {
                // console.log('listSeacrh===>', store.SEARCHING.LISTING_SEARCH);
                this.setState({ loading: false })
            } else {
                this.setState({ loading: false })
            }
        } catch (error) {
            this.setState({ loading: false })
            console.log('error', error);
        }
    }
    loadMore = async (pageNo) => {
        // let { orderStore } = Store;
        // let params = {
        //     next_page: pageNo
        // }
        // Merging two objects
        // let obj = Object.assign(params, store.SEARCH_OBJ);
        // console.log('merges obj===>>>', obj);
        this.setState({ loadmore: true })
        
        token = await LocalDB.getUserProfile()
        
        console.log(pageNo)
        // console.log(options)
        // let res = await axios.post(pageNo, { a: 10 }, options)
        // console.log(res)


        const params = {
            a: 10
        }

        const response = await fetch(pageNo, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.token
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client 
            body: JSON.stringify(params)// body data type must match "Content-Type" header
        });

        result = await response.json();

        if (result.status == 200) {
            //forEach Loop LoadMore results
            // response.data.listings.forEach((item) => {
            //     data.listings.push(item);
            // })
            var finish, next_page_url, per_page;
            result.data.data.forEach((item) => {
                // console.log(item)
                finish = this.state.data.push(item)
                next_page_url = result.data.next_page_url,
                    per_page = this.state.per_page + result.data.per_page

            })
            this.setState({
                // data: finish,
                next_page_url: next_page_url,
                per_page: per_page,
                loadmore: false
            })
            // data.pagination = response.data.pagination;
            // this.setState({ })
        } else {
            this.setState({ loadmore: false })
            // Toast.show(response.data.no_more)
        }
        this.setState({ reCaller: false })
    }
    isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };
    _sortingModule = async (item, options) => {
        // store.SEARCH_OBJ = {};
        options.forEach(option => {
            if (option.key === item.key) {
                store.SEARCH_OBJ.sort_by = item.key;
                this.setState({ sorting: false })
                this.sortedApi()
                // console.warn('object===>>>',store.SEARCH_OBJ);
            } else {
                option.checkStatus = false;
            }
        })
    }
    sortedApi = async () => {
        let { orderStore } = Store;
        try {
            this.setState({ loading: true })
            //API calling
            let response = await ApiController.post('listing-search', store.SEARCH_OBJ);
            orderStore.SEARCHING.LISTING_SEARCH = response;
            // console.log('sorting Response===>>>',orderStore.SEARCHING.LISTING_SEARCH);
            if (response.success === true) {
                // console.log('listSeacrh===>', store.SEARCHING.LISTING_SEARCH);
                this.setState({ loading: false })
            } else {
                this.setState({ loading: false })
            }
        } catch (error) {
            this.setState({ loading: false })
            console.log('error', error);
        }
    }
    render() {

        // let { orderStore } = Store;
        // let list = orderStore.SEARCHING.LISTING_SEARCH.data;
        // let data = store.SEARCHING.LISTING_FILTER.data;
        // let home = orderStore.home.homeGet.data.advanced_search;
        // console.warn('pageno=>',orderStore.SEARCHING.LISTING_SEARCH.data.pagination.next_page);
        // console.log('listSeacrhReturn===>', store.SEARCHING.LISTING_SEARCH);
        // console.log(this.state.data)
        return (
            <View style={styles.container}>
                <View style={{ height: height(10), width: width(100), backgroundColor: 'black', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: height(7), width: width(90), backgroundColor: COLOR_PRIMARY, borderRadius: 5, flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={{ width: width(80), alignSelf: 'stretch', paddingHorizontal: 10 }}
                            placeholder='What Are You Looking For?'
                            // placeholderTextColor={COLOR_SECONDARY}
                            value={this.state.search}
                            autoCorrect={true}
                            autoFocus={true}
                            returnKeyType='search'
                            onChangeText={(value) => {
                                this.setState({ search: value });
                            }}
                        />
                        <TouchableOpacity onPress={() => { this.getSearchList() }}>
                            <Image source={require('../../images/searching-magnifying.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain' }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ height: height(8), width: width(100), backgroundColor: 'rgba(0,0,0,0.1)', flexDirection: 'row', borderColor: 'white', borderWidth: 0, alignItems: 'center' }}>
                    <TouchableOpacity style={{ height: height(5), width: width(33.3), flexDirection: 'row', borderRightWidth: 1, borderRightColor: 'rgba(241,241,241,0.2)', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('AdvanceSearch', { navigateToScreen: this.navigateToScreen, getSearchList: this.getSearchList })}>
                        <Image source={require('../../images/filterNew.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain', marginHorizontal: 5 }} />
                        <Text style={{ color: '#010101', fontSize: totalSize(1.8), fontWeight: '400' }}>Filter</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ height: height(5), width: width(33.3), flexDirection: 'row', borderRightWidth: 1, borderRightColor: 'rgba(241,241,241,0.2)', justifyContent: 'center', alignItems: 'center' }} onPress={this._sort} >
                        {/* <Image source={require('../../images/SortNew.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain', marginHorizontal: 5 }} />
                        <Text style={{ color: '#010101', fontSize: totalSize(1.8), fontWeight: '400' }}>Sort</Text> */}
                    </TouchableOpacity>
                    <TouchableOpacity style={{ height: height(5), width: width(33.3), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.resetSearchList()}>
                        <Image source={require('../../images/reload.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain', marginHorizontal: 5 }} />
                        <Text style={{ color: '#010101', fontSize: totalSize(1.8), fontWeight: '400' }}>Reset</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.data !== "" ?
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            {
                                this.state.loading ?
                                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <ActivityIndicator size='large' color='red' animating={true} />
                                    </View>
                                    :
                                    <ScrollView
                                        showsVerticalScrollIndicator={false}
                                        onScroll={({ nativeEvent }) => {
                                            if (this.isCloseToBottom(nativeEvent)) {
                                                if (this.state.reCaller === false) {
                                                    this.loadMore(this.state.next_page_url);
                                                }
                                                this.setState({ reCaller: true })
                                            }
                                        }}
                                        scrollEventThrottle={400}>
                                        <View style={{ height: height(6), width: width(100), backgroundColor: 'white', justifyContent: 'center', marginBottom: 5, alignItems: 'flex-start' }}>
                                            <Text style={{ fontSize: totalSize(2.2), color: COLOR_SECONDARY, marginHorizontal: 15, marginVertical: 10 }}>{this.state.per_page} results found</Text>
                                        </View>
                                        {
                                            this.state.data.data !== "" ?
                                                this.state.data.map((item, key) => {
                                                    return (
                                                        <ListingComponent item={item} key={key} listStatus={true} />
                                                    )
                                                })
                                                : null
                                        }
                                        {
                                            0 ?
                                                <View style={{ height: height(7), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
                                                    {
                                                        this.state.loadmore ?
                                                            <ActivityIndicator size='large' color='red' animating={true} />
                                                            : null
                                                    }
                                                </View>
                                                :
                                                null
                                        }
                                    </ScrollView>
                            }
                        </View>
                        :
                        this.state.loading ?
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size='large' color='red' animating={true} />
                            </View>
                            :
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text>No Listing</Text>
                            </View>
                }
                <Modal
                    animationInTiming={200}
                    animationOutTiming={100}
                    animationIn="slideInLeft"
                    animationOut="slideOutRight"
                    avoidKeyboard={true}
                    transparent={true}
                    isVisible={this.state.sorting}
                    onBackdropPress={() => this.setState({ sorting: false })}
                    style={{ flex: 1 }}>
                    <View style={{ height: height(5 + (home_listings.length * 6)), width: width(90), alignSelf: 'center', backgroundColor: COLOR_PRIMARY }}>
                        <View style={{ height: height(7), width: width(90), flexDirection: 'row', borderBottomWidth: 0.5, alignItems: 'center', borderBottomColor: '#c4c4c4' }}>
                            <View style={{ height: height(5), width: width(80), justifyContent: 'center' }}>
                                <Text style={{ fontSize: totalSize(2), fontWeight: '500', color: COLOR_SECONDARY, marginHorizontal: 10 }}>Sort By</Text>
                            </View>
                            <TouchableOpacity style={{ height: height(3.5), width: width(6), justifyContent: 'center', alignItems: 'center', backgroundColor: 'red' }} onPress={() => { this._sort() }}>
                                <Image source={require('../../images/clear-button.png')} style={{ height: height(2), width: width(3), resizeMode: 'contain' }} />
                            </TouchableOpacity>
                        </View>
                        {
                            home_listings.map((item, key) => {
                                return (
                                    <TouchableOpacity key={key} style={{ height: height(5), width: width(90), flexDirection: 'row', justifyContent: 'center' }}
                                    // onPress={() => { this._sortingModule(item, home_listings), item.checkStatus = !item.checkStatus }}
                                    >
                                        <View style={{ height: height(6), width: width(80), justifyContent: 'center', alignItems: 'flex-start' }}>
                                            <Text style={{ fontSize: totalSize(1.6), color: item.checkStatus ? 'red' : COLOR_SECONDARY, marginHorizontal: 10 }}>{item.value}</Text>
                                        </View>
                                        <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center' }}>
                                            <CheckBox
                                                size={16}
                                                uncheckedColor={COLOR_GRAY}
                                                checkedColor='red'
                                                containerStyle={{ backgroundColor: 'transparent', width: width(10), borderWidth: 0 }}
                                                checked={item.checkStatus}
                                            // onPress={() => { this._sortingModule(item, home_listings), item.checkStatus = !item.checkStatus }}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                );
                            })
                        }
                    </View>
                </Modal>
            </View>
        );

        // let { orderStore } = Store;
        // let list = orderStore.SEARCHING.LISTING_SEARCH.data;
        // let data = store.SEARCHING.LISTING_FILTER.data;
        // let home = orderStore.home.homeGet.data.advanced_search;
        // // console.warn('pageno=>',orderStore.SEARCHING.LISTING_SEARCH.data.pagination.next_page);
        // // console.log('listSeacrhReturn===>', store.SEARCHING.LISTING_SEARCH);
        // return (
        //     <View style={styles.container}>
        //         <View style={{ height: height(10), width: width(100), backgroundColor: store.settings.data.navbar_clr, justifyContent: 'center', alignItems: 'center' }}>
        //             <View style={{ height: height(7), width: width(90), backgroundColor: COLOR_PRIMARY, borderRadius: 5, flexDirection: 'row', alignItems: 'center' }}>
        //                 <TextInput
        //                     style={{ width: width(80), alignSelf: 'stretch', paddingHorizontal: 10 }}
        //                     placeholder={home.search_placeholder}
        //                     // placeholderTextColor={COLOR_SECONDARY}
        //                     value={this.state.search}
        //                     autoCorrect={true}
        //                     autoFocus={store.moveToSearch ? false : false}
        //                     returnKeyType='search'
        //                     onChangeText={(value) => {
        //                         this.setState({ search: value });
        //                     }}
        //                 />
        //                 <TouchableOpacity onPress={() => { this.getSearchList() }}>
        //                     <Image source={require('../../images/searching-magnifying.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain' }} />
        //                 </TouchableOpacity>
        //             </View>
        //         </View>
        //         <View style={{ height: height(8), width: width(100), backgroundColor: 'rgba(0,0,0,0.9)', flexDirection: 'row', borderColor: 'white', borderWidth: 0, alignItems: 'center' }}>
        //             <TouchableOpacity style={{ height: height(5), width: width(33.3), flexDirection: 'row', borderRightWidth: 1, borderRightColor: 'rgba(241,241,241,0.2)', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('AdvanceSearch', { navigateToScreen: this.navigateToScreen, getSearchList: this.getSearchList })}>
        //                 <Image source={require('../../images/filterNew.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain', marginHorizontal: 5 }} />
        //                 <Text style={{ color: 'white', fontSize: totalSize(1.8), fontWeight: '400' }}>{home.filter}</Text>
        //             </TouchableOpacity>
        //             <TouchableOpacity style={{ height: height(5), width: width(33.3), flexDirection: 'row', borderRightWidth: 1, borderRightColor: 'rgba(241,241,241,0.2)', justifyContent: 'center', alignItems: 'center' }} onPress={this._sort} >
        //                 <Image source={require('../../images/SortNew.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain', marginHorizontal: 5 }} />
        //                 <Text style={{ color: 'white', fontSize: totalSize(1.8), fontWeight: '400' }}>{home.sort}</Text>
        //             </TouchableOpacity>
        //             <TouchableOpacity style={{ height: height(5), width: width(33.3), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.resetSearchList()}>
        //                 <Image source={require('../../images/reload.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain', marginHorizontal: 5 }} />
        //                 <Text style={{ color: 'white', fontSize: totalSize(1.8), fontWeight: '400' }}>{home.reset}</Text>
        //             </TouchableOpacity>
        //         </View>
        //         {
        //             list !== "" ?
        //                 <View style={{ flex: 1, alignItems: 'center' }}>
        //                     {
        //                         this.state.loading ?
        //                             <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        //                                 <ActivityIndicator size='large' color={store.settings.data.navbar_clr} animating={true} />
        //                             </View>
        //                             :
        //                             <ScrollView
        //                                 showsVerticalScrollIndicator={false}
        //                                 onScroll={({ nativeEvent }) => {
        //                                     if (this.isCloseToBottom(nativeEvent)) {
        //                                         if (this.state.reCaller === false) {
        //                                             this.loadMore(list.pagination.next_page);
        //                                         }
        //                                         this.setState({ reCaller: true })
        //                                     }
        //                                 }}
        //                                 scrollEventThrottle={400}>
        //                                 <View style={{ height: height(6), width: width(100), backgroundColor: 'white', justifyContent: 'center', marginBottom: 5, alignItems:'flex-start' }}>
        //                                     <Text style={{ fontSize: totalSize(2.2), color: COLOR_SECONDARY, marginHorizontal: 15, marginVertical: 10 }}>{list.total_listings}</Text>
        //                                 </View>
        //                                 {
        //                                     list !== "" ?
        //                                         list.listings.map((item, key) => {
        //                                             return (
        //                                                <ListingComponent item={item} key={key} listStatus={true} />
        //                                             )
        //                                         })
        //                                         : null
        //                                 }
        //                                 {
        //                                     list.pagination.has_next_page ?
        //                                         <View style={{ height: height(7), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
        //                                             {
        //                                                 this.state.loadmore ?
        //                                                     <ActivityIndicator size='large' color={store.settings.data.navbar_clr} animating={true} />
        //                                                     : null
        //                                             }
        //                                         </View>
        //                                         :
        //                                         null
        //                                 }
        //                             </ScrollView>
        //                     }
        //                 </View>
        //                 :
        //                 this.state.loading ?
        //                     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        //                         <ActivityIndicator size='large' color={store.settings.data.navbar_clr} animating={true} />
        //                     </View>
        //                     :
        //                     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        //                         <Text>{orderStore.SEARCHING.LISTING_SEARCH.message}</Text>
        //                     </View>
        //         }
        //         <Modal
        //             animationInTiming={200}
        //             animationOutTiming={100}
        //             animationIn="slideInLeft"
        //             animationOut="slideOutRight"
        //             avoidKeyboard={true}
        //             transparent={true}
        //             isVisible={this.state.sorting}
        //             onBackdropPress={() => this.setState({ sorting: false })}
        //             style={{ flex: 1 }}>
        //             <View style={{ height: height(5 + (data.sorting.option_dropdown.length * 6)), width: width(90), alignSelf: 'center', backgroundColor: COLOR_PRIMARY }}>
        //                 <View style={{ height: height(7), width: width(90), flexDirection: 'row', borderBottomWidth: 0.5, alignItems: 'center', borderBottomColor: '#c4c4c4' }}>
        //                     <View style={{ height: height(5), width: width(80), justifyContent: 'center' }}>
        //                         <Text style={{ fontSize: totalSize(2), fontWeight: '500', color: COLOR_SECONDARY, marginHorizontal: 10 }}>{data.sorting.title}</Text>
        //                     </View>
        //                     <TouchableOpacity style={{ height: height(3.5), width: width(6), justifyContent: 'center', alignItems: 'center', backgroundColor: store.settings.data.navbar_clr }} onPress={() => { this._sort() }}>
        //                         <Image source={require('../../images/clear-button.png')} style={{ height: height(2), width: width(3), resizeMode: 'contain' }} />
        //                     </TouchableOpacity>
        //                 </View>
        //                 {
        //                     data.sorting.option_dropdown.map((item, key) => {
        //                         return (
        //                             <TouchableOpacity key={key} style={{ height: height(5), width: width(90), flexDirection: 'row', justifyContent: 'center' }}
        //                                 onPress={() => { this._sortingModule(item, data.sorting.option_dropdown), item.checkStatus = !item.checkStatus }}
        //                             >
        //                                 <View style={{ height: height(6), width: width(80), justifyContent: 'center',alignItems:'flex-start' }}>
        //                                     <Text style={{ fontSize: totalSize(1.6), color: item.checkStatus ? store.settings.data.navbar_clr : COLOR_SECONDARY, marginHorizontal: 10 }}>{item.value}</Text>
        //                                 </View>
        //                                 <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center' }}>
        //                                     <CheckBox
        //                                         size={16}
        //                                         uncheckedColor={COLOR_GRAY}
        //                                         checkedColor={store.settings.data.navbar_clr}
        //                                         containerStyle={{ backgroundColor: 'transparent', width: width(10), borderWidth: 0 }}
        //                                         checked={item.checkStatus}
        //                                         onPress={() => { this._sortingModule(item, data.sorting.option_dropdown), item.checkStatus = !item.checkStatus }}
        //                                     />
        //                                 </View>
        //                             </TouchableOpacity>
        //                         );
        //                     })
        //                 }
        //             </View>
        //         </Modal>
        //     </View>
        // );
    }
}
