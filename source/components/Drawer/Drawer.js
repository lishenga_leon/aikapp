import React from 'react';
import { Platform, I18nManager } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { FONT_NORMAL, COLOR_PRIMARY, COLOR_ORANGE, COLOR_TRANSPARENT_BLACK } from '../../../styles/common';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import Home from '../Home/Home';
import FeatureDetailTabBar from '../FeatureDetail/FeatureDetailTabBar';
import UserDashboard from '../UserDashboard/UserDashboard';
import SideMenu from './SideMenu';
import EditProfile from '../UserDashboard/EditProfile'
import ContactUs from '../ContactUs/ContactUs';
import AboutUs from '../AboutUs/AboutUs';
import Terms from '../Settings/TermsandConditions';
import Privacy from '../Settings/Privacy';
import EventsTabs from '../Events/EventsTabs';
import SearchingScreen from '../AdvanceSearch/SearchingScreen';
import Find from '../FindEntry/Find';

const DashboardStack = createStackNavigator({
  UserDashboard: UserDashboard,
  EditProfile: EditProfile,

});
const DrawerComp = createDrawerNavigator({
  Home: Home,
  FeatureDetailTabBar: FeatureDetailTabBar,
  Dashboard: DashboardStack,
  AboutUs: AboutUs,
  ContactUs: ContactUs,
  EventsTabs: EventsTabs,
  SearchingScreen: SearchingScreen,
  Privacy: Privacy,
  Terms: Terms,
  Find: Find
},
  {
    order: [  'Home', 'SearchingScreen', 'Dashboard', 'Terms', 'Privacy', 'AboutUs', 'ContactUs', 'EventsTabs' ],
    initialRouteName: 'Home',
    drawerWidth: width(80), 
    drawerPosition: I18nManager.isRTL ? 'right' : 'left',
    mode: Platform.OS === 'ios' ? 'modal' : 'card',
    contentComponent: props => <SideMenu {...props} />,
    useNativeAnimations: true,
    drawerBackgroundColor: COLOR_TRANSPARENT_BLACK,
    contentOptions: {
      activeTintColor: COLOR_PRIMARY,
      activeBackgroundColor: COLOR_ORANGE,
      inactiveTintColor: COLOR_PRIMARY,
      itemsContainerStyle: {
        justifyContent: 'center',
      },
      itemStyle: {
        // ImageBackground:'red'
      },
      labelStyle: {
        fontFamily: FONT_NORMAL,
        fontSize: totalSize(1.6)
      },
      activeLabelStyle: {
        fontFamily: 'bold',
        fontSize: totalSize(1.9)
      },
      inactiveLabelStyle: {
        fontFamily: FONT_NORMAL,
        fontSize: totalSize(1.6)
      },
      iconContainerStyle: {
        opacity: 1,
        // backgroundColor:'red'
      }
    }
  }
);
export default DrawerComp;
