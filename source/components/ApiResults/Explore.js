import React, { Component } from 'react';
import {
    Text, View, Image, ImageBackground, TouchableOpacity, I18nManager,
    ScrollView, TextInput, ActivityIndicator, TouchableWithoutFeedback, RefreshControl
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { width, height, totalSize } from 'react-native-dimension';
import { COLOR_PRIMARY, COLOR_GRAY, COLOR_SECONDARY } from '../../../styles/common';
import Modal from "react-native-modal";
import { CheckBox } from 'react-native-elements';
import styles from '../../../styles/Home';
import Toast from 'react-native-simple-toast'
import ListingComponent from '../Home/ListingComponent';
import LocalDB from '../../LocalDB/LocalDB';

const home_listings = [
    {
        listing_id: 1,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 2,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 3,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 4,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 5,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 6,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
    {
        listing_id: 7,
        category_name: 'First Item',
        image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
        color_code: 'red',
        business_hours_status: '',
        listing_title: 'Arkansas, United States',
        rating_stars: 4,
        total_views: 2,
        posted_date: 12 / 12 / 2014,
        color_code: 'blue'
    },
];



export default class Explore extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            reCaller: false,
            loading: false,
            loadmore: false,
            sorting: false,
            sortCheck: false,
            search: '',
            data: '',
            param: {},
            next_page_url: '',
            per_page: '',
            arr: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }]
        }
    }
    static navigationOptions = { header: null };
    navigateToScreen = (route, title) => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.setParams({ otherParam: title });
        this.props.navigation.dispatch(navigateAction);
    }


    _sort = () => this.setState({ sorting: !this.state.sorting })


    componentDidMount = async () => {
        this.setState({ loading: false })
        await this.getSearchList()
    }
    getSearchList = async () => {

        if(this.state.data.length){
            this.setState({
                data: ''
            })
        }

        token = await LocalDB.getUserProfile()

        this.setState({ loading: true })
       
        var result, params;
        if (this.props.navigation.getParam('otherParam', 'SearchingScreen') && this.props.navigation.getParam('agroName', 'SearchingScreen') == null) {

            params = {
                enterprise: this.props.navigation.getParam('enterpriceName', 'SearchingScreen')
            }

            const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(params)// body data type must match "Content-Type" header
            });

            result = await response.json();


        }  else if (this.props.navigation.getParam('categoryId', 'SearchingScreen')) {

            params = {
                enterprise: this.props.navigation.getParam('categoryId', 'SearchingScreen'),
            }

            const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(params)// body data type must match "Content-Type" header
            });

            result = await response.json();

        } else if (this.props.navigation.getParam('subcategoryId', 'SearchingScreen')) {

            params = {
                category: this.props.navigation.getParam('subcategoryId', 'SearchingScreen')
            }

            const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(params)// body data type must match "Content-Type" header
            });

            result = await response.json();

        }else {

            params = {
                a: 10
            }

            const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.token
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *client 
                body: JSON.stringify(params)// body data type must match "Content-Type" header
            });

            result = await response.json();

        }

        console.log(result)

        if (result.status == 200) {
            if (result.data.to == 1) {
                this.setState({
                    loading: false,
                    data: result.data.data,
                    next_page_url: result.data.next_page_url,
                    per_page: result.data.total,
                    param: params
                })
            } else if(result.data.to == null){
                this.setState({
                    loading: false,
                    data: result.data.data,
                    next_page_url: result.data.next_page_url,
                    per_page: 0,
                    param: params
                })
            }
            else {
                this.setState({
                    loading: false,
                    data: result.data.data,
                    next_page_url: result.data.next_page_url,
                    per_page: result.data.total,
                    param: params
                })
            }
        } else {
            this.setState({ loading: false })
            Toast.show('Kindly try again later', Toast.TOP, Toast.LONG)
        }
    }


    searchSpecific = async(value) =>{

        token = await LocalDB.getUserProfile()
  
        var result;

        const params = {
            name: value
        }

        const response = await fetch('http://209.97.136.165:8600/api/listings/search/listing', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.token
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client 
            body: JSON.stringify(params)// body data type must match "Content-Type" header
        });

        result = await response.json();

        if (result.status == 200) {
            this.setState({
                loading: false,
                data: result.data.data,
                next_page_url: result.data.next_page_url,
                per_page: result.data.total
            })
        } else {
            Toast.show('Kindly try again later', Toast.LONG, Toast.TOP)
            this.setState({ loading: false })
        }
    }


    loadMore = async (pageNo) => {

        this.setState({ loadmore: true })
        
        token = await LocalDB.getUserProfile()

        const response = await fetch(pageNo, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.token
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client 
            body: JSON.stringify(this.state.param)// body data type must match "Content-Type" header
        });

        result = await response.json();

        if (result.status == 200) {

            var next_page_url, per_page;
            result.data.data.forEach((item) => {
                finish = this.state.data.push(item)
                next_page_url = result.data.next_page_url,
                    per_page = result.data.total

            })
            this.setState({
                next_page_url: next_page_url,
                per_page: per_page,
                loadmore: false
            })
        } else {
            this.setState({ loadmore: false })
            Toast.show('Kindly try again later', Toast.LONG, Toast.TOP)
        }
        this.setState({ reCaller: false })
    }

    isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };


    render() {
        return (
            <View style={styles.container}>
                <View style={{ height: height(10), width: width(100), backgroundColor: 'black', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: height(7), width: width(90), backgroundColor: COLOR_PRIMARY, borderRadius: 5, flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={{ width: width(80), alignSelf: 'stretch', paddingHorizontal: 10 }}
                            placeholder='What Are You Looking For?'
                            // placeholderTextColor={COLOR_SECONDARY}
                            value={this.state.search}
                            autoCorrect={true}
                            autoFocus={true}
                            returnKeyType='search'
                            onChangeText={(value) => {
                                this.setState({ loading: true, search: value })
                                this.searchSpecific(value);
                            }}
                        />
                        <TouchableWithoutFeedback onPress={() => { this.searchSpecific(this.state.search) }}>
                            <Image source={require('../../images/searching-magnifying.png')} style={{ height: height(3), width: width(5), resizeMode: 'contain' }} />
                        </TouchableWithoutFeedback>
                    </View>
                </View>
                {
                    this.state.data !== "" ?
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            {
                                this.state.loading ?
                                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <ActivityIndicator size='large' color='red' animating={true} />
                                    </View>
                                    :
                                    <ScrollView
                                        showsVerticalScrollIndicator={false}
                                        onScroll={({ nativeEvent }) => {
                                            if (this.isCloseToBottom(nativeEvent)) {
                                                if (this.state.reCaller === false) {
                                                    this.loadMore(this.state.next_page_url);
                                                }
                                                this.setState({ reCaller: true })
                                            }
                                        }}
                                        refreshControl={
                                            <RefreshControl
                                                colors={['white']}
                                                progressBackgroundColor='red'
                                                tintColor='red'
                                                refreshing={this.state.loading}
                                                onRefresh={() => this.getSearchList()}
                                            />
                                        }
                                        scrollEventThrottle={400}>
                                        <View style={{ height: height(6), width: width(100), backgroundColor: 'white', justifyContent: 'center', marginBottom: 5, alignItems: 'flex-start' }}>
                                            <Text style={{ fontSize: totalSize(2.2), color: COLOR_SECONDARY, marginHorizontal: 15, marginVertical: 10 }}>{this.state.per_page} results found</Text>
                                        </View>
                                        {
                                            this.state.data.data !== "" ?
                                                this.state.data.map((item, key) => {
                                                    return (
                                                        <ListingComponent item={item} key={key} listStatus={true} />
                                                    )
                                                })
                                                : null
                                        }
                                        {
                                            this.state.loadmore ?
                                                <View style={{ height: height(7), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
                                                    {
                                                        this.state.loadmore ?
                                                            <ActivityIndicator size='large' color='red' animating={true} />
                                                            : null
                                                    }
                                                </View>
                                                :
                                                null
                                        }
                                    </ScrollView>
                            }
                        </View>
                        :
                        this.state.loading ?
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size='large' color='red' animating={true} />
                            </View>
                            :
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text>No Listing</Text>
                            </View>
                }
                <Modal
                    animationInTiming={200}
                    animationOutTiming={100}
                    animationIn="slideInLeft"
                    animationOut="slideOutRight"
                    avoidKeyboard={true}
                    transparent={true}
                    isVisible={this.state.sorting}
                    onBackdropPress={() => this.setState({ sorting: false })}
                    style={{ flex: 1 }}>
                    <View style={{ height: height(5 + (home_listings.length * 6)), width: width(90), alignSelf: 'center', backgroundColor: COLOR_PRIMARY }}>
                        <View style={{ height: height(7), width: width(90), flexDirection: 'row', borderBottomWidth: 0.5, alignItems: 'center', borderBottomColor: '#c4c4c4' }}>
                            <View style={{ height: height(5), width: width(80), justifyContent: 'center' }}>
                                <Text style={{ fontSize: totalSize(2), fontWeight: '500', color: COLOR_SECONDARY, marginHorizontal: 10 }}>Sort By</Text>
                            </View>
                            <TouchableOpacity style={{ height: height(3.5), width: width(6), justifyContent: 'center', alignItems: 'center', backgroundColor: 'red' }} onPress={() => { this._sort() }}>
                                <Image source={require('../../images/clear-button.png')} style={{ height: height(2), width: width(3), resizeMode: 'contain' }} />
                            </TouchableOpacity>
                        </View>
                        {
                            home_listings.map((item, key) => {
                                return (
                                    <TouchableOpacity key={key} style={{ height: height(5), width: width(90), flexDirection: 'row', justifyContent: 'center' }}
                                    // onPress={() => { this._sortingModule(item, home_listings), item.checkStatus = !item.checkStatus }}
                                    >
                                        <View style={{ height: height(6), width: width(80), justifyContent: 'center', alignItems: 'flex-start' }}>
                                            <Text style={{ fontSize: totalSize(1.6), color: item.checkStatus ? 'red' : COLOR_SECONDARY, marginHorizontal: 10 }}>{item.value}</Text>
                                        </View>
                                        <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center' }}>
                                            <CheckBox
                                                size={16}
                                                uncheckedColor={COLOR_GRAY}
                                                checkedColor='red'
                                                containerStyle={{ backgroundColor: 'transparent', width: width(10), borderWidth: 0 }}
                                                checked={item.checkStatus}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                );
                            })
                        }
                    </View>
                </Modal>
            </View>
        );
    }
}
