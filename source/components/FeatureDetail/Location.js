import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View, Button, Image, ImageBackground, TouchableOpacity, I18nManager,
  ScrollView, TextInput, FlatList, ActivityIndicator
} from 'react-native';
import Modal from "react-native-modal";
import { width, height, totalSize } from 'react-native-dimension';
import MapView, { Polyline, Marker, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import { FONT_NORMAL, FONT_BOLD, COLOR_PRIMARY, COLOR_ORANGE, COLOR_GRAY, COLOR_SECONDARY } from '../../../styles/common';
import { observer } from 'mobx-react';
// import Store from '../../Stores';
import Claim from './Claim'
import Report from './Report'
import styles from '../../../styles/LocationStyleSheet';
import FeatureDetail from './FeatureDetail';
import Geocoder from 'react-native-geocoding';
import { withNavigation } from 'react-navigation';


class Location extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      reportModel: false,
      isClaimVisible: false,
      region: ''
    }
    I18nManager.forceRTL(false);
  }


  componentDidMount = () => {
    const details = this.props.navigation.getParam('item', 'FeatureDetailTabBar')
    Geocoder.init("AIzaSyAWLQgl-Pv6qz5yrQ6YpMe8Zqkyzebh6cs");

    Geocoder.from(details.street_address+ 'kenya')
      .then(json => {
        var location = json.results[0].geometry.location;
        this.setState({
          region: {
            latitude: location.lat,
            longitude: location.lng,
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          }
        })
      })
      .catch(error => {
        this.setState({
          region: {
            latitude: -1.2328358764732026,
            longitude: 36.72482997179032,
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          }
        })
      });
  }


  setModalVisible = (state, prop) => {
    if (state === 'claim' && prop === false) {
      this.setState({ reportModel: false, isClaimVisible: true })
    } else {
      if (state === 'report') {
        this.setState({ reportModel: true, isClaimVisible: false })
      }
    }
  }
  hideModels = (state, hide) => {
    if (state === 'claim') {
      this.setState({ isClaimVisible: hide, reportModel: hide })
    } else {
      if (state === 'report') {
        this.setState({ reportModel: hide, reportModel: hide })
      }
    }
  }
  static navigationOptions = {
    header: null,
  };
  render() {

    if (this.state.region == '') {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size='large' color='red' animating={true} />
        </View>
      )
    }else{
      return (
        <View style={styles.container}>
          <ScrollView>
            <FeatureDetail callModel={this.setModalVisible} />
            <View style={styles.mapCon}>
              <MapView
                ref={(ref) => this.mapView = ref}
                showsScale={true}
                rotateEnabled={true}
                scrollEnabled={false}
                zoomEnabled={true}
                zoomControlEnabled={true}
                toolbarEnabled={false}
                showsCompass={true}
                showsBuildings={true}
                showsIndoors={true}
                provider={PROVIDER_GOOGLE}
                showsMyLocationButton={true}
                minZoomLevel={5}
                maxZoomLevel={20}
                mapType={"standard"}
                animateToRegion={{
                  latitude: 31.582045,
                  longitude: 74.329376,
                  latitudeDelta: 0.00922 * 1.5,
                  longitudeDelta: 0.00421 * 1.5
                }}
                style={styles.map}
                region={this.state.region}
              // onRegionChange={this.onRegionChange.bind(this)}
              >
                {
                  1 ?
                    <MapView.Marker
                      coordinate={this.state.region}
                      // title={'Current location'}
                      // description={'I am here'}
                      pinColor={'#3edc6d'}
                    />
                    : <View></View>
                }
              </MapView>
            </View>
          </ScrollView>
          <Modal
            animationInTiming={500}
            animationIn="slideInLeft"
            animationOut="slideOutRight"
            avoidKeyboard={true}
            // transparent={false}
            isVisible={this.state.reportModel}
            onBackdropPress={() => this.setState({ reportModel: false })}
            style={{ flex: 1 }}>
            <Report hideModels={this.hideModels} />
          </Modal>
          <Modal
            animationInTiming={500}
            animationIn="slideInLeft"
            animationOut="slideOutRight"
            avoidKeyboard={true}
            // transparent={false}
            isVisible={this.state.isClaimVisible}
            onBackdropPress={() => this.setState({ isClaimVisible: false })}
            style={{ flex: 1 }}>
            <Claim hideModels={this.hideModels} />
          </Modal>
        </View>
      );
    }
  }
}

export default withNavigation(Location)