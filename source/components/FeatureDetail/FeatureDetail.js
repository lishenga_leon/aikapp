import React, { Component } from 'react';
import { Platform, Text, View, ActivityIndicator, Image, ImageBackground } from 'react-native';
import { width } from 'react-native-dimension';
import * as Animatable from 'react-native-animatable';
import Form from 'react-native-vector-icons/AntDesign'
import StarRating from 'react-native-star-rating';
import Toast from 'react-native-simple-toast'
import { observer } from 'mobx-react';
import ApiController from '../../ApiController/ApiController';
import styles from '../../../styles/FeatureDetailStyle';
import { withNavigation } from 'react-navigation';


class FeatureDetail extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }
  static navigationOptions = {
    header: null,
  };
  render() {

    const details = this.props.navigation.getParam('item', 'FeatureDetailTabBar')

    return (
      <View>
        {
          details.category_id == 305 ?
            <ImageBackground source={require('../../images/images/Agrodealer-icon.png')} style={styles.bgImg}>
              <View style={styles.imgConView}>
                <View style={styles.imgSubConView}>
                  <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                    <Text style={styles.title}>{details.company_name}</Text>
                  </View>
                </View>
              </View>
            </ImageBackground> :
            details.category_id == 126 ?
              <ImageBackground source={require('../../images/images/fertiliser-icon.png')} style={styles.bgImg}>
                <View style={styles.imgConView}>
                  <View style={styles.imgSubConView}>
                    <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                      <Text style={styles.title}>{details.company_name}</Text>
                    </View>
                  </View>
                </View>
              </ImageBackground> :
              details.category_id == 122 ?
                <ImageBackground source={require('../../images/images/Machinery-icon.png')} style={styles.bgImg}>
                  <View style={styles.imgConView}>
                    <View style={styles.imgSubConView}>
                      <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                        <Text style={styles.title}>{details.company_name}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground> :
                details.category_id == 105 ?
                  <ImageBackground source={require('../../images/images/agronomists-icon.png')} style={styles.bgImg}>
                    <View style={styles.imgConView}>
                      <View style={styles.imgSubConView}>
                        <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                          <Text style={styles.title}>{details.company_name}</Text>
                        </View>
                      </View>
                    </View>
                  </ImageBackground> :
                  details.category_id == 304 ?
                    <ImageBackground source={require('../../images/images/Vets-icon.png')} style={styles.bgImg}>
                      <View style={styles.imgConView}>
                        <View style={styles.imgSubConView}>
                          <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                            <Text style={styles.title}>{details.company_name}</Text>
                          </View>
                        </View>
                      </View>
                    </ImageBackground> :
                    details.category_id == 106 ?
                      <ImageBackground source={require('../../images/images/animal-feeds-icon.png')} style={styles.bgImg}>
                        <View style={styles.imgConView}>
                          <View style={styles.imgSubConView}>
                            <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                              <Text style={styles.title}>{details.company_name}</Text>
                            </View>
                          </View>
                        </View>
                      </ImageBackground> :
                      details.category_id == 135 ?
                        <ImageBackground source={require('../../images/images/Irrigation-icon.png')} style={styles.bgImg}>
                          <View style={styles.imgConView}>
                            <View style={styles.imgSubConView}>
                              <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                                <Text style={styles.title}>{details.company_name}</Text>
                              </View>
                            </View>
                          </View>
                        </ImageBackground> :
                        details.category_id == 147 ?
                          <ImageBackground source={require('../../images/images/seeds-icon.png')} style={styles.bgImg}>
                            <View style={styles.imgConView}>
                              <View style={styles.imgSubConView}>
                                <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                                  <Text style={styles.title}>{details.company_name}</Text>
                                </View>
                              </View>
                            </View>
                          </ImageBackground>
                          : <ImageBackground source={{ uri: 'http://drommonster.co.za/wp-content/uploads/2018/02/Blog2.jpg' }} style={styles.bgImg}>
                          <View style={styles.imgConView}>
                            <View style={styles.imgSubConView}>
                              <View style={{ width: width(90), justifyContent: 'center', flexWrap: 'wrap', alignItems: 'flex-start' }}>
                                <Text style={styles.title}>{details.company_name}</Text>
                              </View>
                            </View>
                          </View>
                        </ImageBackground>
        }
      </View>
    );
  }
}

export default withNavigation(FeatureDetail)