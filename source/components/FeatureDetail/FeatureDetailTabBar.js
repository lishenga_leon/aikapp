import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import React, { Component } from 'react';
import { View, Dimensions, Text } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { INDICATOR_SIZE, INDICATOR_VISIBILITY, OVERLAY_COLOR, TEXT_SIZE, TEXT_COLOR, ANIMATION, COLOR_PRIMARY, COLOR_ORANGE, COLOR_GRAY, COLOR_SECONDARY, S18, S2 } from '../../../styles/common';
import { withNavigation } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';
import Description from './Description';
import Location from './Location';


const FirstRoute = () => (
  <Description />
);

const SecondRoute = () => (
  <Location />
);

const renderScene = SceneMap({
  description: FirstRoute,
  location: SecondRoute,
});

class FeatureDetailTabBar extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      controlTab: true,
      dis: 'Discription',
      write: 'Write a Review',
      routes: [
        { key: 'description', title: 'Description' },
        { key: 'location', title: 'Location' },
      ],
      index: 0,
      location: true,
      details: ''
    }
  }
  
  static navigationOptions = ({ navigation }) => ({
    headerTitle: navigation.state.params.list_title,
    headerTintColor: 'white',
    headerTitleStyle: {
      fontSize: totalSize(2),
      fontWeight: 'normal'
    },
    headerStyle: {
      backgroundColor: '#009900'
    }
  });


  componentDidMount = async () => {
    if(this.props.navigation.getParam('item', 'FeatureDetailTabBar') == undefined){
      this.setState({ loading: true })
    }else{
      this.setState({ loading: false })
    }
  }


  route = () => {
    
    if (this.state.controlTab) {
      this.state.routes.push({ key: 'description', title: 'Description' });
    }else{
      if (this.state.location) {
        this.state.routes.push({ key: 'location', title: 'Location' });
      }
    }
  }


  _renderLabel = ({ route }) => (
    <Text style={{ fontSize: 13, color: COLOR_SECONDARY }}>{route.title}</Text>
  );


  render() {


    // let { orderStore } = Store;
    // let data = orderStore.settings.data;
    // var main_clr = store.settings.data.main_clr;
    if (this.state.loading == true) {
      return (
        <View style={{ height: height(100), width: width(100), flex: 1 }}>
          <Spinner
            visible={INDICATOR_VISIBILITY}
            // textContent={data.loading_txt}
            size={INDICATOR_SIZE}
            cancelable={true}
            color='red'
            animation={ANIMATION}
            overlayColor={OVERLAY_COLOR}
          // textStyle={{ fontSize: totalSize(TEXT_SIZE), color: TEXT_COLOR }}
          />
        </View>
      );
    }
    return (
      <TabView
        navigationState={{
          index: this.state.index,
          routes: this.state.routes
        }}
        renderScene={renderScene}
        swipeEnabled={true}
        animationEnabled={true}
        onIndexChange={index => this.setState({ index: index })}
        initialLayout={{ width: Dimensions.get('window').width }}
        // canJumpToTab={true}
        tabBarPosition='bottom'
        renderTabBar={props =>
          <TabBar
            {...props}
            // getLabelText={()=> this.label()}
            // renderLabel={()=> this.label()}
            renderLabel={this._renderLabel}
            scrollEnabled={true}
            bounces={true}
            useNativeDriver={true}
            pressColor='red'
            style={{ height: 45, justifyContent: 'center', backgroundColor: COLOR_PRIMARY, borderTopWidth: 0.2, borderColor: COLOR_GRAY, shadowColor: COLOR_GRAY }}
            labelStyle={{ color: COLOR_SECONDARY }}
            tabStyle={{}}
            indicatorStyle={{ backgroundColor: 'red', height: 3 }}
            activeLabelStyle={{ color: 'red' }}
          />

        }
      />
    );
  }
}

export default withNavigation(FeatureDetailTabBar)