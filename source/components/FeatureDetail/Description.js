import React, { Component } from 'react';
import {
  Text, View, Image, TouchableOpacity, ScrollView, Linking, Platform
} from 'react-native';
import Modal from "react-native-modal";
import call from 'react-native-phone-call';
import { width, height, totalSize } from 'react-native-dimension';
import Accordion from 'react-native-collapsible/Accordion';
import { Avatar, Icon } from 'react-native-elements';
import CountDown from 'react-native-countdown-component';
import ImageViewer from 'react-native-image-zoom-viewer';
import HTMLView from 'react-native-htmlview';
import * as Animatable from 'react-native-animatable';
import { COLOR_PRIMARY, COLOR_ORANGE, COLOR_GRAY, COLOR_SECONDARY, S14 } from '../../../styles/common';
import IconPhone from 'react-native-vector-icons/MaterialCommunityIcons';
import IconUser from 'react-native-vector-icons/Fontisto';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconLock from 'react-native-vector-icons/Entypo';
// import Store from '../../Stores';
// import store from '../../Stores/orderStore';
import Claim from './Claim'
import Report from './Report'
import styles from '../../../styles/DescriptionStyleSheet';
import FeatureDetail from './FeatureDetail';
import { withNavigation } from 'react-navigation';



const images = [
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  },
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  },
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  },
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  },
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  },
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  },
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  },
  {
    uri: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=802&q=80',
  }
]

class Description extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      getCoupon: false,
      reportModel: false,
      modalVisible: false,
      isClaimVisible: false,
      images: [],
      index: 0,
      timer: 0,
      web: false,
    }
  }
  
  static navigationOptions = {
    header: null
  };


  call = (number) => {
    const args = {
      number: number, // String value with the number to call
      prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
    }

    call(args).catch(console.error)
  }

  webSite = (url) => {
    if (url !== "") {
      Linking.openURL(url);
    }
  }

  setModalVisible = (state, prop) => {
    if (state === 'claim' && prop === false) {
      this.setState({ reportModel: false, isClaimVisible: true })
    } else {
      if (state === 'report') {
        this.setState({ reportModel: true, isClaimVisible: false })
      }
    }
  }


  hideModels = (state, hide) => {
    if (state === 'claim') {
      this.setState({ isClaimVisible: hide, reportModel: hide })
    } else {
      if (state === 'report') {
        this.setState({ reportModel: hide, reportModel: hide })
      }
    }
  }


  countDown(eventDate) {
    var eventDate = new Date(eventDate);
    var currentDate = new Date();
    var differenceTravel = eventDate.getTime() - currentDate.getTime();
    var seconds = Math.floor((differenceTravel) / (1000));
    this.setState({ timer: seconds })
  }


  _renderHeader = (section, isActive) => {
    let { orderStore } = Store;
    let data = orderStore.home.FEATURE_DETAIL.data.listing_detial;
    return (
      <View style={[styles.labelCon, { borderBottomWidth: 0 }]}>
        <Animatable.View
          duration={1000}
          animation="tada"
          easing="ease-out"
          iterationCount={'infinite'}
          direction="alternate">
          <Image source={require('../../images/clock.png')} style={styles.labelIcon} />
        </Animatable.View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
          <Text style={[styles.timeTxt, { color: data.color_code }]}>{data.business_hours_status}</Text>
        </View>
        <Image source={(isActive ? require('../../images/dropDown.png') : require('../../images/next.png'))} style={styles.dropDownIcon} />
      </View>
    );
  }


  _renderContent = (section, content) => {
    let { orderStore } = Store;
    let data = orderStore.home.FEATURE_DETAIL.data.listing_detial;
    return (
      <View style={styles.renderCon}>
        {
          data.listing_hours.map((item, key) => {
            return (
              <View key={key} style={styles.renderStrip}>
                <View style={styles.stripDay}>
                  {item.current_day !== true ? <Text style={styles.renderDayTxt}>{item.day_name}</Text> : <Text style={[styles.renderDayTxt, { color: '#32cb55', fontWeight: 'bold' }]}>{item.day_name}</Text>}
                </View>
                <View style={styles.stripTime}>
                  {item.current_day !== true ? <Text style={styles.renderTimeTxt}>{item.is_closed === true ? "closed" : item.timingz}</Text> : <Text style={[styles.renderTimeTxt, { color: '#32cb55', fontWeight: 'bold' }]}>{item.is_closed === true ? "closed" : item.timingz}</Text>}
                </View>
              </View>
            );
          })
        }
      </View>
    );
  }
  render() {

    const details = this.props.navigation.getParam('item', 'FeatureDetailTabBar')

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <FeatureDetail callModel={this.setModalVisible} />
          {
            details.street_address !== null && details.street_address !== "NULL" ?
              <TouchableOpacity style={styles.labelCon}>
                <Image source={require('../../images/address.png')} style={styles.labelIcon} />
                <View style={styles.labeTxtCon}>
                  <Text style={styles.labelTxt}>{details.street_address}</Text>
                </View>
              </TouchableOpacity>
              : null
          }
          {
            details.postal_address !== null && details.postal_address !== "NULL" ?
              <TouchableOpacity style={styles.labelCon}>
                <IconLock name='address' color='black' size={30} style={[styles.labelIcon, { marginTop: height(2) }]} />
                <View style={styles.labeTxtCon}>
                  <Text style={styles.labelTxt}>{details.postal_address}</Text>
                </View>
              </TouchableOpacity>
              : null
          }
          {
            details.phone_number !== null && details.phone_number !== "NULL" ?
              <TouchableOpacity style={styles.labelCon}>
                <Image source={require('../../images/call.png')} style={styles.labelIcon} />
                <View style={styles.labeTxtCon}>
                  <Text style={styles.labelTxt}>{details.phone_number}</Text>
                </View>
              </TouchableOpacity>
              : null
          }
          {
            details.email_address !== null && details.email_address !== "NULL" ?
              <TouchableOpacity style={styles.labelCon}>
                <IconPhone name='email' color='black' size={30} style={[styles.labelIcon, { marginTop: height(2) }]} />
                <View style={styles.labeTxtCon}>
                  <Text style={styles.labelTxt}>{details.email_address}</Text>
                </View>
              </TouchableOpacity>
              : null
          }
          {
            details.contact_person_name !== null && details.contact_person_name !== "NULL" ?
              <TouchableOpacity style={styles.labelCon}>
                <IconUser name='person' color='black' size={30} style={[styles.labelIcon, { marginTop: height(2) }]} />
                <View style={styles.labeTxtCon}>
                  <Text style={styles.labelTxt}>{details.contact_person_name}</Text>
                </View>
              </TouchableOpacity>
              : null
          }
          {
            details.contact_person !== null && details.contact_person !== "NULL" ?
              <TouchableOpacity style={styles.labelCon}>
                <IconAnt name='phone' color='black' size={30} style={[styles.labelIcon, { marginTop: height(2) }]} />
                <View style={styles.labeTxtCon}>
                  <Text style={styles.labelTxt}>{details.contact_person}</Text>
                </View>
              </TouchableOpacity>
              : null
          }
          <Text style={styles.titleTxt}>Description</Text>
          <View style={{ width: width(88), alignSelf: 'center', marginHorizontal: 15, marginBottom: 10, justifyContent: 'center' }} >
            {
              details.category_id == 305 ?
                <HTMLView
                  value={'We stock the best variety of seeds, animal feed, fertilizers, veterinary products and supplements, drip irrigation kits, farming tools, herbicides, insecticides and hay. Other services include soil testing and animal healthcare.'}
                  stylesheet={styles.longTxt}
                /> :
                details.category_id == 126 ?
                  <HTMLView
                    value={'To improve productivity, we advice and supply different fertilizers for different regions and soil types. Weather organic or chemical fertilizer, we have the product to improve your soil fertility.'}
                    stylesheet={styles.longTxt}
                  /> :
                  details.category_id == 122 ?
                    <HTMLView
                      value={'We sell Tractors. We also offer machinery such as planters, disc plough, sprayer, cultivator, combine harvester and rotary tiller with our tractors. We have a reliable after-service programme for all our products.'}
                      stylesheet={styles.longTxt}
                    /> :
                    details.category_id == 105 ?
                      <HTMLView
                        value={'Services include soil sampling, laboratory result analysis, farm management advisory, farm inputs recommendations, advise and interpretation of property mapping, soil surveys and local area vegetation. Others include Financing options, post-harvest handling and access to markets.'}
                        stylesheet={styles.longTxt}
                      /> :
                      details.category_id == 304 ?
                        <HTMLView
                          value={'Consultancy for the following services; Clinical (treatment of diseased animals and control of production limiting disorders); Preventive (avoid disease outbreak); Provision of drugs, vaccines and artificial insemination; Human health protection (inspection of marketed animal products).'}
                          stylesheet={styles.longTxt}
                        /> :
                        details.category_id == 106 ?
                          <HTMLView
                            value={'We produce various types of feeds, that is, roughages, concentrates and mixed feeds. Roughages include pasture forages, hays, silages and byproduct. Concentrates are the energy-rich grains and molasses, vitamin and mineral supplements. Mixed feeds offer complete balanced rations.'}
                            stylesheet={styles.longTxt}
                          /> :
                          details.category_id == 135 ?
                            <HTMLView
                              value={'We are specialists in surface, drip, sprinkler, center pivot irrigation systems. We stock pipes, fittings, valves, sprinklers, meters, solar panels and other equipment.'}
                              stylesheet={styles.longTxt}
                            /> :
                            details.category_id == 147 ?
                              <HTMLView
                                value={'We distribute highly quality, high yield certified seeds for most crops. Weather hybrid for maize, cotton and oilseeds or vegetable seeds for beans, cabbage, carrot, peppers, french beans, kale, onion, tomatoes etc we have it all.'}
                                stylesheet={styles.longTxt}
                              />
                              : null
            }
          </View>
        </ScrollView>
        <Modal
          animationInTiming={200}
          animationOutTiming={100}
          animationIn="slideInLeft"
          animationOut="slideOutRight"
          avoidKeyboard={true}
          transparent={true}
          isVisible={this.state.getCoupon}
          onBackdropPress={() => this.setState({ getCoupon: false })}
          style={{ flex: 1 }}>
          <View style={{ height: height(40), width: width(90), alignSelf: 'center', backgroundColor: COLOR_PRIMARY }}>
            <View style={{ flex: 1 }}>
              <View style={{ height: height(4), alignItems: 'flex-end' }}>
                <TouchableOpacity style={{ elevation: 3, height: height(3.5), width: width(6), justifyContent: 'center', alignItems: 'center', backgroundColor: 'red' }} onPress={() => { this.setState({ getCoupon: false }) }}>
                  <Image source={require('../../images/clear-button.png')} style={{ height: height(2), width: width(3), resizeMode: 'contain' }} />
                </TouchableOpacity>
              </View>
              <View style={{ alignItems: 'center', marginVertical: 5 }}>
                <Text style={{ fontSize: totalSize(1.7), color: 'black', marginTop: 0, fontWeight: 'bold' }}>AIK APP</Text>
                <Text style={{ fontSize: totalSize(1.5), color: 'gray', textAlign: 'center', marginHorizontal: 10, marginVertical: 5, }}>AIK APP</Text>
                <Text style={{ fontSize: totalSize(1.6), color: 'black' }}>AIK APP</Text>
              </View>
              <Text selectable={true} style={{ height: height(6), marginHorizontal: 20, padding: 10, marginVertical: 5, textAlign: 'center', borderStyle: 'dashed', borderRadius: 5, borderWidth: 1, borderColor: 'red', backgroundColor: COLOR_PRIMARY, color: COLOR_SECONDARY, fontSize: totalSize(1.6) }}>AIK APP</Text>
              <Text style={{ fontSize: totalSize(1.5), color: 'gray', marginVertical: 5, alignSelf: 'center' }}>AIK APP</Text>
              <Text style={{ fontSize: totalSize(1.5), color: 'gray', marginVertical: 5, alignSelf: 'center' }}>12/12/2014 12/12/2014</Text>
            </View>
            <TouchableOpacity style={{ elevation: 3, height: height(6), justifyContent: 'center', alignItems: 'center', backgroundColor: 'red' }} onPress={() => { this.setState({ reportModel: false }), this.webSite('https://www.lanthanion.com') }}>
              <Text style={{ fontSize: totalSize(1.8), color: COLOR_PRIMARY, fontWeight: 'bold' }}>12/12/2014</Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <Modal
          animationInTiming={500}
          animationIn="slideInLeft"
          animationOut="slideOutRight"
          avoidKeyboard={true}
          // transparent={false}
          isVisible={this.state.reportModel}
          onBackdropPress={() => this.setState({ reportModel: false })}
          style={{ flex: 1 }}>
          <Report hideModels={this.hideModels} />
        </Modal>
        <Modal
          animationInTiming={500}
          animationIn="slideInLeft"
          animationOut="slideOutRight"
          avoidKeyboard={true}
          // transparent={false}
          isVisible={this.state.isClaimVisible}
          onBackdropPress={() => this.setState({ isClaimVisible: false })}
          style={{ flex: 1 }}>
          <Claim hideModels={this.hideModels} />
        </Modal>
        <Modal
          visible={this.state.modalVisible}
          transparent={true}
          style={{ flex: 1 }}
          onRequestClose={() => this.setState({ modalVisible: false })}
        >
          <ImageViewer
            imageUrls={images}
            index={this.state.index}
            pageAnimateTime={500}
            backgroundColor='black'
            transparent={false}
            enablePreload={true}
            style={{ flex: 1, backgroundColor: 'black' }}
            footerContainerStyle={{ marginHorizontal: 0, marginVertical: 0 }}
            onDoubleClick={() => {
              this.setState({ modalVisible: false })
            }}
            onSwipeDown={() => {
              this.setState({ modalVisible: false })
              // console.log('onSwipeDown');
            }}
            enableSwipeDown={true}
          />
        </Modal>
      </View>
    );
  }
}
export default withNavigation(Description)
