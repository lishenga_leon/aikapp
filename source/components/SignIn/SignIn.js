import React, { Component } from 'react';
import {
  ActivityIndicator, Text, View,
  Image, TouchableOpacity,
  TextInput, ScrollView
} from 'react-native';
import { INDICATOR_SIZE } from '../../../styles/common';
import { width, height } from 'react-native-dimension';
import Toast from 'react-native-simple-toast';
import IconUser from 'react-native-vector-icons/Octicons';
import IconLock from 'react-native-vector-icons/SimpleLineIcons';
import styles from '../../../styles/SignIn';
import LocalDB from '../../LocalDB/LocalDB';
import { Icon } from 'native-base';




export default class SignIn extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      password: '',//12345
      email: '',//usama@gmail.com
      loginError: false,
      icon: 'eye-off',
      showPassword: true
    }
  }
  static navigationOptions = {
    header: null
  };

  login = async () => {
    // let { orderStore } = Store;
    this.setState({ loading: true, loginError: false })
    let params = {
      email: this.state.email,
      password: this.state.password
    }

    const response = await fetch('http://209.97.136.165:8600/api/auth/login', {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client
      body: JSON.stringify(params) // body data type must match "Content-Type" header
    });

    result = await response.json();

    if (result.status == 200) {

      await LocalDB.saveProfile(this.state.email, this.state.password, result.data);
      this.setState({
        loading: false,
        email: '',
        password: ''
      })
      Toast.showWithGravity('Success', Toast.LONG, Toast.TOP)
      this.props.navigation.replace('Drawer');
    } else {
      this.setState({ loading: false, loginError: true, email: '', password: '' })
    }
  }

  _changeIcon() {
    this.setState(prevState => ({
      icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
      showPassword: !prevState.showPassword
    }))
  }

  render() {

    return (
      <ScrollView style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(5), width: width(100), flexDirection: 'row' }}>
            <TouchableOpacity style={styles.bckImgCon} onPress={() => this.props.navigation.goBack()}>
              <Image source={require('../../images/back_btn.png')} style={styles.backBtn} />
            </TouchableOpacity>
          </View>
          <View style={styles.logoView}>
            <Image source={require('../../images/images/AIK-App-Logo.png')} style={styles.logoImg} />
          </View>
          <View style={styles.buttonView}>
            <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
              <View style={{ marginHorizontal: 10 }}>
                <IconUser name='mail' color='black' size={24} />
              </View>
              <View style={{ flex: 4.1 }}>
                <TextInput
                  onChangeText={(value) => this.setState({ email: value.toLowerCase() })}
                  underlineColorAndroid='transparent'
                  placeholder='Your Email Address'
                  placeholderTextColor='black'
                  textContentType='emailAddress'
                  keyboardType='email-address'
                  underlineColorAndroid='transparent'
                  autoCorrect={true}
                  value={this.state.email}
                  style={[styles.inputTxt, { textAlign: 'left' }]}
                />
              </View>
            </View>
            <View style={styles.btn} onPress={() => { this.props.navigation.navigate('Login') }}>
              <View style={{ marginHorizontal: 10 }}>
                <IconLock name='lock' color='black' size={24} />
              </View>
              <View style={{ flex: 4.1 }}>
                <TextInput
                  onChangeText={(value) => this.setState({ password: value })}
                  underlineColorAndroid='transparent'
                  placeholder='Your Password'
                  secureTextEntry={this.state.showPassword}
                  placeholderTextColor='black'
                  underlineColorAndroid='transparent'
                  value={this.state.password}
                  autoCorrect={true}
                  style={styles.inputTxt}
                />
              </View>
              <View style={{ marginHorizontal: 10 }}>
                <Icon name={this.state.icon} color='black' size={24} onPress={() => this._changeIcon()} />
              </View>
            </View>
            {
              this.state.loginError ?
                <View>
                  <Text style={styles.loginErrorTxt}>Kindly provide the right credentials</Text>
                </View>
                :
                null
            }
            <TouchableOpacity style={[styles.signUpBtn, { backgroundColor: 'rgba(0,153,0,0.9)' }]} onPress={() => this.login()}>
              <Text style={styles.signUpTxt}>Sign In</Text>
            </TouchableOpacity>
            <View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
              {!this.state.loading ? null :
                <ActivityIndicator size={INDICATOR_SIZE} color='red' animating={true} hidesWhenStopped={true} />}
            </View>
          </View>
          <View style={styles.footer}>
            <Text style={styles.forgetpwrd} onPress={() => this.props.navigation.navigate('ForgetPassword')}>Forgot Password</Text>
            <Text style={styles.newHere} onPress={() => this.props.navigation.navigate('SignUp')}>New Here? Register With Us</Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}
