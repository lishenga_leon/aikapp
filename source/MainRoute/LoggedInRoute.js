import React, { Component } from 'react';
import { Platform, StatusBar, Text, View, TouchableOpacity, Image, Alert } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
//Authentication
import Splash from '../components/SplashScreen/Splash';
import MainScreen from '../components/MainScreen/MainScreen';
import SignUp from '../components/SignUp/SignUp';
import SignIn from '../components/SignIn/SignIn';
import ForgetPassword from '../components/ForgetPassword/ForgetPassword';

import Home from '../components/Home/Home';
import FeatureDetail from '../components/FeatureDetail/FeatureDetail';
import Descriptions from '../components/Events/Descriptions';
import FeatureDetailTabBar from '../components/FeatureDetail/FeatureDetailTabBar';
import Discription from '../components/FeatureDetail/Description';
import UserDashboard from '../components/UserDashboard/UserDashboard';
import Drawer from '../components/Drawer/Drawer';
import SideMenu from '../components/Drawer/SideMenu';
import ContactUs from '../components/ContactUs/ContactUs';
import AboutUs from '../components/AboutUs/AboutUs';
import EditProfile from '../components/UserDashboard/EditProfile'
import SignUpPage2 from '../components/SignUp/SignUpPage2'
import AdvanceSearch from '../components/AdvanceSearch/AdvanceSearch';
import SearchingScreen from '../components/AdvanceSearch/SearchingScreen';
import Agrodealer from '../components/ApiResults/Agrodealer';
import Agronomist from '../components/ApiResults/Agronomist';
import Animal from '../components/ApiResults/Animal';
import Fertilizer from '../components/ApiResults/Fertilizer';
import Irrigation from '../components/ApiResults/Irrigation';
import Machinery from '../components/ApiResults/Machinery';
import Veterinarian from '../components/ApiResults/Veterinarian';
import Find from '../components/FindEntry/Find';
import Seeds from '../components/ApiResults/Seeds';
import Explore from '../components/ApiResults/Explore';
import EventsTabs from '../components/Events/EventsTabs';
import styles from '../../styles/HeadersStyles/DrawerHeaderStyleSheet';

const RootStack = createStackNavigator(
  {
    // Splash: Splash,
    MainScreen: MainScreen,
    SignUp: SignUp,
    SignIn: SignIn,
    Home: Home,
    ForgetPassword: ForgetPassword,
    FeatureDetail: FeatureDetail,
    Descriptions: Descriptions,
    Discription: Discription,
    FeatureDetailTabBar: FeatureDetailTabBar,
    Find: Find,
    Drawer: {
      screen: Drawer,
      navigationOptions: ({ navigation }) => ({
        title: navigation.getParam('otherParam', 'Home'),
        header: (
          <View style={[styles.overlyHeader,{ backgroundColor: 'rgba(0,153,0,0.9)' }]}>
            <TouchableOpacity style={styles.drawerBtnCon} onPress={() => {
              // Alert.alert(navigation+'')
              navigation.toggleDrawer()
              // navigation.openDrawer()
            }}>
              <Image source={require('../images/drawer.png')} style={styles.drawerBtn} />
            </TouchableOpacity>
            <View style={styles.headerTxtCon}>
              <Text style={styles.headerTxt}>{navigation.getParam('otherParam', 'Home')}</Text>
            </View>
            <View style={{flex:1}}></View>
            {/* <Image source={require('../images/search_white.png')} style={styles.headerSearch} />
            <Image source={require('../images/cart.png')} style={styles.cart} /> */}
          </View>
        )
      }),
    },
    SideMenu: SideMenu,
    UserDashboard: UserDashboard,
    ContactUs: ContactUs,
    AboutUs: AboutUs,
    EditProfile: EditProfile,
    SignUpPage2: SignUpPage2,
    Agronomist: Agronomist,
    Agrodealer: Agrodealer,
    Veterinarian: Veterinarian,
    Fertilizer: Fertilizer,
    Animal: Animal,
    Irrigation: Irrigation,
    Machinery: Machinery,
    Explore: Explore,
    Seeds: Seeds,
    AdvanceSearch: AdvanceSearch,
    SearchingScreen: SearchingScreen,
    EventsTabs: EventsTabs,
  },
  {
    initialRouteName: 'Drawer',
    mode: Platform.OS === 'ios' ? 'pattern' : 'card',
    headerMode: Platform.OS === 'ios' ? 'float' : 'screen',
    headerTransitionPreset: Platform.OS === 'ios' ? 'uikit' : null,
    headerLayoutPreset: Platform.OS === 'ios' ? 'center' : 'left',
    headerBackTitleVisible: Platform.OS === 'ios' ? true : false,
    navigationOptions: {
      headerTitleStyle: { fontSize: totalSize(2), fontWeight: 'normal' },
      gesturesEnabled: true,
    }
  }
);
export default createAppContainer(RootStack);