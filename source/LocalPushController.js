import React, { Component } from 'react';
import { Platform, SafeAreaView, StatusBar, NetInfo, BackHandler, Alert, Image } from 'react-native';
import Route from './MainRoute/Route';
import LoggedInRoute from './MainRoute/LoggedInRoute';
// import store from './Stores/orderStore';
import { MenuProvider } from 'react-native-popup-menu';
import LocalDB from './LocalDB/LocalDB';
import SplashScreen from 'react-native-splash-screen';
import { width, height, totalSize } from 'react-native-dimension';
import PushNotification from 'react-native-push-notification'

PushNotification.configure({
    // (required) Called when a remote or local notification is opened or received
    // onNotification: function (notification) {
    //     console.log('LOCAL NOTIFICATION ==>', notification)
    //     // this.props.navigation.navigate('FeatureDetails')
    // },
    // popInitialNotification: true,
    // requestPermissions: true
})

export const LocalNotification = () => {
    PushNotification.localNotification({
        autoCancel: true,
        bigText:
            'The event is the Largest Horticultural Fair in Africa and has consistently grown over the years. This event is in its 18th year and attracts an audience from across the continent and Europe. The event show-cases products and services from stake-holders in the horticultural industry (primarily flower industry but also car manufacturers, accessories, financial institutions etc).',
        subText: 'Naivasha Sports Club - Naivasha ',
        title: 'Naivasha Horticultural Fair',
        message: 'Expand me to see more',
        vibrate: true,
        vibration: 300,
        playSound: true,
        soundName: 'default',
        actions: '["Yes", "No"]'
    })
}
