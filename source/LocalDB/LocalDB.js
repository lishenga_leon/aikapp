import { Alert } from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

class LocalDB {

    static async saveProfile(email, password, data) {
        console.warn('data==>>', data);
        try {
            await AsyncStorage.setItem('email', email);
            await AsyncStorage.setItem('password', password);
            await AsyncStorage.setItem('profile', JSON.stringify(data));
            return true;
        } catch (error) {
            // Error retrieving data
            console.warn(error.message);
            return false;
        }
    }

    static async getUserProfile() {
        let item = {};
        try {
            var emial = await AsyncStorage.getItem('email') || null;
            var password = await AsyncStorage.getItem('password') || null;
            item = await AsyncStorage.getItem('profile') || null;
            const userProfile = JSON.parse(item);
            return userProfile;
        }
        catch (error) {
            console.warn(error.message);
            return null;
        }
    }

    static async getUserEmail() {
        let item = {};
        try {
            var email = await AsyncStorage.getItem('email') || null;
            return email;
        }
        catch (error) {
            console.warn(error.message);
            return null;
        }
    }

    static async saveIsProfilePublic(isPublic) {
        try {
            await AsyncStorage.setItem('isPublic', isPublic);
        }
        catch (error) {
            console.warn(error.message);
        }
    }
    static async isProfilePublic() {

        const isPublic = await AsyncStorage.getItem('isPublic') || null;
        if (isPublic === '1')
            return true;
        else
            return false;
    }

    static storeDeviceToken(token, id) {

        try {
            AsyncStorage.setItem('deviceToken', JSON.stringify(token.token));
            return true;
        } catch (error) {
            // Error retrieving data
            console.warn(error.message);
            return false;
        }
    }

    static async getDeviceToken() {
        let item = {};
        try {
            var token = await AsyncStorage.getItem('deviceToken') || null;
            console.log(token)
            return JSON.parse(token);
        }
        catch (error) {
            console.warn(error.message);
            return null;
        }
    }

    static notificationStatus(status, data) {

        try {
            AsyncStorage.setItem('notificationStatus', status);
            AsyncStorage.setItem('notificationData', JSON.stringify(data));
            return true;
        } catch (error) {
            // Error retrieving data
            console.warn(error.message);
            return false;
        }
    }

    static async getNotificationStatus() {
        try {
            var status = await AsyncStorage.getItem('notificationStatus') || null;
            var data = await AsyncStorage.getItem('notificationData') || null;
            return { status: status, data: JSON.parse(data)};
        }
        catch (error) {
            console.warn(error.message);
            return null;
        }
    }

}

export default LocalDB; 