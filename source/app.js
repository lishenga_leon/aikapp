import React, { Component } from 'react';
import { Platform, SafeAreaView, StatusBar, NetInfo, BackHandler, Alert, Image } from 'react-native';
import Route from './MainRoute/Route';
import LoggedInRoute from './MainRoute/LoggedInRoute';
// import store from './Stores/orderStore';
import { MenuProvider } from 'react-native-popup-menu';
import LocalDB from './LocalDB/LocalDB';
import SplashScreen from 'react-native-splash-screen';
import { width, height, totalSize } from 'react-native-dimension';
import Toast from 'react-native-simple-toast';
import PushNotification from 'react-native-push-notification'
import { LocalNotification } from './LocalPushController'
import axios from 'axios';

export default class AppMain extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      color: 'black',
      loading: false,
      userEmail: 'EMPTY',
      deviceToken: null
    };
  }
  fucn() {
    var timerId = setInterval(() => {
      if (1) {
        // this.setState({ loading: false })
        clearInterval(timerId);
      } else {
        console.warn('app.js')
      }
    }, 5000);
  }
  componentDidMount = async () => {
    // PushNotification.configure({
    //   // (optional) Called when Token is generated (iOS and Android)
    //   onRegister: function (token) {
    //     console.log('TOKEN:', token)
    //     const params = {
    //       user_id: 0,
    //       token: token.token
    //     }

    //     console.log(params)

    //     axios.post('http://209.97.136.165:8600/api/push/register', params)
    //       .then(function (response) {
    //         console.log(response);
    //       })
    //       .catch(function (error) {
    //         console.log(error);
    //       });
    //   },
    //   // (required) Called when a remote or local notification is opened or received
    //   onNotification: function (notification) {
    //     console.log('REMOTE NOTIFICATION ==>', notification)
    //     // process the notification here
    //     LocalDB.notificationStatus(false, notification['data'])
    //   },
    //   // Android only: GCM or FCM Sender ID
    //   senderID: '369360035878',
    //   popInitialNotification: true,
    //   requestPermissions: true
    // })
    userEmail = await LocalDB.getUserEmail()
    this.setState({
      userEmail: userEmail
    })
    setTimeout(() => { this.setState({ color: '#009900' }) }, 9000)
  }



  registerToken = async () => {
    usertoken = await LocalDB.getUserProfile()

    //API calling
    var result;

    const response = await fetch('http://209.97.136.165:8600/api/auth/getuser', {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client 
    });

    result = await response.json();

    console.log(result)

    if (result.status == 200) {
      console.log('ajda')
      PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
          console.log('TOKEN:', token)
          LocalDB.storeDeviceToken(token, result.data.id)
        },
        // (required) Called when a remote or local notification is opened or received
        onNotification: function (notification) {
          console.log('REMOTE NOTIFICATION ==>', notification)
          // process the notification here
        },
        // Android only: GCM or FCM Sender ID
        senderID: '256218572662',
        popInitialNotification: true,
        requestPermissions: true
      })

    } else {
      Toast.show('Kindly try again later', Toast.BOTTOM, Toast.LONG)
    }
  }



  render() {
    if (this.state.userEmail == 'EMPTY') {
      return (
        <SafeAreaView style={{
          flex: 1,
          backgroundColor: '#009900',
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Image source={require('./images/images/AIK-App-Logo.png')} style={{
            height: height(7.7),
            width: width(60),
            resizeMode: 'contain',
          }} />
        </SafeAreaView>
      )
    } else {
      if (this.state.userEmail !== null) {
        SplashScreen.hide()
        return (
          <SafeAreaView style={{ flex: 1, backgroundColor: this.state.color }}>
            <LoggedInRoute />
          </SafeAreaView>
        );
      } else {
        SplashScreen.hide()
        return (
          <SafeAreaView style={{ flex: 1, backgroundColor: this.state.color }}>
            <Route />
          </SafeAreaView>
        );
      }
    }
  }
}
